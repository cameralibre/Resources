[FFmpeg](https://ffmpeg.org) does EVERYTHING. Convert videos, de- and re-mux, make lossless screencasts, split concatenate clips and frames, convert from basically any codec to any other, trim a video file without re-encoding, process and transform video and audio, batch process clips, inspect and edit video properties… EVERYTHING. You just have to learn to love the command line :) Documentation is [here](https://ffmpeg.org/documentation.html), it’s worth having a dig through!

These are probably the commands I use most often.

## Convert video to WebM
 
Useful for being able to send a tiny reference file for others to check. 

    ffmpeg -i inputVideo.mp4 -acodec libvorbis -aq 4 -ac 2 -qmax 23 -threads 4 -s 640x360 ouputVideo.webm

**ffmpeg** calls the FFmpeg program

**-i** provides an input file, **inputVideo.mp4**

**-acodec** sets the audio codec to Ogg Vorbis, using the '**libvorbis**' library

**-aq** sets audio quality to **4** (high quality). this ranges from 7 (best) to 3 (worst).

**-ac** sets the number of audio channels to **2** 

**-qmax** sets the maximum video quality to aim for - bit rate will be variable depending on your video's content. This ranges from 15 (best) to 45 (worst) **23** is high quality, but not the best.

**-threads** sets the number of CPU threads to use to **4**, ensuring a fast encode (4 is the maximum on my laptop)

**-s** sets the size to a tiny **640x360**px 

and finally I state the name of my desired output file: **ouputVideo.webm**


## Cut video file into a smaller clip

You can use the time offset parameter (-ss) to specify the start time stamp in HH:MM:SS.ms format while the -t parameter is for specifying the actual duration of the clip in seconds.

    ffmpeg -i inputVideo.mp4 -ss 00:00:50.0 -codec copy -t 20 ouputVideo.mp4

I work almost exclusively with 25fps material, so if I wanted a clip that was 5 seconds and 12 frames long, that would be written as:

    -t 5.48

Each frame is 0.04 of a second, so just multiply the number of frames by four and you hae a percentage of a second.
If you use 24fps, 29.97, or another framerate, I dunno, use a [framerate calculator](http://www.1728.org/angle.htm)

## Inspect a File

    ffprobe inputVideo.mov

FFprobe is actually a different part of FFmpeg, but it is really useful for finding out the size, bit rate, codec, creation date and other information. You'll get an output like this:

    Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'inputVideo.mov':
    Metadata:
      major_brand     : qt  
      minor_version   : 537331968
      compatible_brands: qt  CAEP
      creation_time   : 2016-04-09 12:00:52
    Duration: 00:00:26.88, start: 0.000000, bitrate: 37666 kb/s
    Stream #0:0(eng): Video: h264 (Constrained Baseline) (avc1 / 0x31637661), yuvj420p(pc, smpte170m/bt709/bt709), 1920x1080, 36127 kb/s, 25 fps, 25 tbr, 25k tbn, 50k tbc (default)
    Metadata:
      creation_time   : 2016-04-09 12:00:52
    Stream #0:1(eng): Audio: pcm_s16le (sowt / 0x74776F73), 48000 Hz, stereo, s16, 1536 kb/s (default)
    Metadata:
      creation_time   : 2016-04-09 12:00:52


## Extract audio from a video file

    ffmpeg -i inputVideo.mp4 -acodec copy ouputAudio.aac



## Convert audio to OGG

The [Ogg Vorbis](http://www.vorbis.com/) format is a high-quality lossy format, which is also unencumbered by patents and open source, so I prefer it to MP3. 

    ffmpeg -i inputAudio.flac -c:a libvorbis -ac 2 -ar 44100 -ab 256k outputAudio.ogg

(note: 256k is quite a high bit rate, it is safe to bring this down considerably, depending on your use case.

## Convert audio to AAC

AAC is the common audio codec to use in an MP4 file - it's generally packaged in either a .aac or .m4a container. FFmpeg has different libraries which it can use to convert AAC files. The proprietary Fraunhofer library is the highest quality, but it is patent-encumbered and isn't packaged with FFmpeg. If you want to use it you will have to build FFmpeg from source.

    ffmpeg -i inputAudio.wav -c:a libfdk_aac -vbr 5 ouputAudio.m4a

To use the experimental AAC library that is included in FFmpeg, you need to explicitly enable its use with **strict -2**:

    ffmpeg -i inputAudio.wav -c:a aac -strict -2 ouputAudio.m4a


## Mux audio + video

Multiplexing different files can be useful for example to replace a sound mix without having to re-export the entire video.

**To mux a video file _that has no audio_, together with an audio file:**

    ffmpeg -i "inputVideoOnly.mp4" -i "inputAudio.m4a" -vcodec copy -acodec copy "ouputVideoWithAudio.mp4"

**To replace the _existing_ audio of a muxed video with a new audio track:**

    ffmpeg -i inputAudio.m4a -i inputVideoWithAudio.mp4 -map 0:0 -map 1:0 -acodec copy -vcodec copy ouputVideoWithAudio.mp4


## Create video from image sequence

For maximum flexibility I find it useful to export animations from Synfig as PNG sequences, and then I can create viewable, shareable video files from these, as well as import the lossless image sequence into Kdenlive for editing. I find that the default settings for MP4 are acceptable, but I have not had good experiences with the default for WebM, which is usually a more useful format for my purposes (uploading reference files to a git repository, sharing videos for others to check, or embedding directly in web pages)

**MP4**

    ffmpeg -i inputImage.%04d.png ouputVideo.mp4  

**WebM**

    ffmpeg -i inputImage.%04d.png -c:v libvpx -b:v 3M ouputVideo.webm 

**ProRes(HQ)**

    ffmpeg -y -probesize 5000000 -f image2 -r 24 -force_fps -i inputImage.%04d.png -c:v prores_ks -profile:v 3 -qscale:v 10 -vendor ap10 -pix_fmt yuv422p10le -s 1920x1080 -r 24 outputVideo.mov

('-vendor ap10' makes video decoders think that this file was created by the expected Apple Quicktime program)

here the formulation %04d refers to an incrementation of file numbers. Synfig renders PNG sequences following this pattern - say I choose 'Animation.png' as the filename to export, Synfig will export each frame of my animation as:

Animation.0001.png

Animation.0002.png

Animation.0003.png

Animation.0004.png

Animation.0005.png

So when I tell FFmpeg to use **Animation.%04d.png** as an input, I am saying "look for filenames which start with **Animation.**, followed by a **4-digit number**, followed by **.png**." FFmpeg will interpret them in ascending order.

This assumes that the sequence starts at 0 - that means that you will need to add "-start_number 0013" between 'ffmpeg' and '-i' if you want to start with frame 13, for example.

## Create GIF from image sequence

This one's a bit trickier because GIFs are an awkward, innefficient and ancient technology - but they're well supported so they'll be sticking around a while longer....
GIF color palettes are only 256 colors, so it's best to analyze your input frames first to work out which 256 colors are most suitable. 

So there's a first pass which generates a 'palette.png' file:

    ffmpeg -y -i Animation.%04d.png \
    -vf scale=640:-1:flags=lanczos,palettegen palette.png

Then you can reference that palette to convert the input files into a GIF:

    ffmpeg -i Animation.%04d.png -i palette.png -filter_complex \
    "scale=320:-1:flags=lanczos[x];[x][1:v]paletteuse" output.gif

## Double the speed of a video

Note: this just throws away every second frame, it's not actually doing frame blending or anything fancy - try [slomovideo](https://github.com/slowmoVideo/slowmoVideo) for that.

    ffmpeg -i inputVideo-100.mp4 -filter:v "setpts=0.5*PTS" ouputVideo-200.mp4FFMPEG

## Split video into multiple parts

This will split the input video into 2 parts – one ending at 10s from the start and the other beginning at 10s and ending at the end of the input video.

    ffmpeg -i inputVideo.mp4 -t 00:00:10 -c copy ouputVideo0To10.mp4 -ss 00:00:10 -codec copy ouputVideo10ToEnd.mp4

## Concatenate multiple video files

create a file, mylist.txt:

	# this is a comment
	file '/path/to/video1'
	file '/path/to/video2'
	file '/path/to/video3'


	ffmpeg -f concat -i mylist.txt -c copy outputVideo.mkv

## Change volume of audio file

To double the audio volume of a file, you can use the 'volume' audio filter.

    ffmpeg -i inputAudio.flac -af 'volume=2' ouputAudio.flac

To halve it, set the filter to 0.5

    ffmpeg -i inputAudio.flac -af 'volume=0.5' ouputAudio.flac

This is pretty rough, as you don't know exactly what level the audio is and, for example if the resulting audio will clip when you double the input, I would usually use Audacity for these tasks - but this is very quick. You can also use dB:

    ffmpeg -i inputAudio.flac -af 'volume=10db' ouputAudio.flac

