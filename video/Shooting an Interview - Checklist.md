### SHOOTING AN INTERVIEW: A Checklist
_CC-BY Sam Muirhead_

## Pre-Production

**Who is your interviewee?** 

 * Research them. What is their background? 
 * Read/watch other interviews with them

**What do you hope to achieve with this video? What information are you trying to convey?**

 * What 
 * Where 
 * When 
 * Who 
 * Why 
 * How 

**Arranging the interview**

Let the subject know: 

 * *why* you want to interview them 
 * *what* information you are interested in 
 * *how* the material will be used and 
 * *how long* the interview will take 

Ask them: 

 * if there is anything specific they feel would be interesting for your particular audience? 
 * can they suggest an interesting /relevant location to the story? *(optional - this only makes sense if they know the location much better than you)*

**What questions should you ask in order to get this information/this story?** 

 * general / open? 
 * specific / technical? 
 * critical? 
 * supportive? 
(usually a combination is best) 

**What shots are you likely to need for the video?** 

 * is the specific location and time of the interview important for the viewer? 
 * does the audience need to see what the person is talking about? 
 * how can this information be illustrated visually?)
 * do you want/need the interviewer in the video? Why?
 * what kind of establishing shots, details or cutaways may be necessary?
 * what other material (photos, diagrams, screenshots, logos) might be useful/necessary? 

**What shots can you feasibly get?**
 
 * if possible, visit the interview location beforehand
 * are there opportunities for filming cutaways and details at, or near, the interview location?
 * How much time will you have available on location - make sure you plan for extra setup and pack-down time!

**What technical requirements are there to this shoot/location?** 

 * how many people will be interviewed?
 * how much light is available? Will it change?
 * What about ambient noise levels? Traffic/machine noise/wind?
 * is it a controlled or uncontrolled environment? 
 * is there enough space to film?
 * will your gear be safe and secure?
 * are there too many distractions?

**The Ideal Location:** 

 * relevant to story 
 * comfortable for subject 
 * technically/aesthetically suitable for filming 

## The Shoot

As much technical set-up as possible should be done first, *without the interviewee*. Let them know how much time you'll need to set up, and that you'll come and get them when everything's ready. 

**Before subject arrives:**

 * choose specific location / background 
 * arrange backgrounds, props, furniture if necessary 
 * choose camera angle(s) 
 * set up lighting/reflectors
 * prepare and *double check* camera 
 * prepare and *double check* sound 
 * do you have full batteries in your devices, and backups ready?
 * do you have space on your recoding medium, and more standing by?

**Camera:**
 
 * check white balance, settings, iris etc.
 * follow composition guidelines (rule of thirds, headroom, looking space, etc) 
 * no 'crossing the line' - imagine the camera is a theatre audience, and your subjects are on stage – the camera should stay 'in the audience' and not cross to the other side of the speakers - it's unsettling and confusing.
 * vary your shot sizes 
 * camera at eye level, or maybe slightly above 
 * subject should look at interviewer, just off camera 
 * is your lens clean?

**Three-Point Lighting:** 

 * key light (strong, main light, perhaps at 45° to the main camera angle), considerably higher than the subject.
 * fill light (often a reflector - softens the harsh shadows from the key light)
 * back light (highlights and separates subject from background)

**Sound:**
 
 * get close! how close? *as close as you possibly can!*
 * listen for strange hums and noises - can they be reduced/eliminated? (eg turn off refrigerator, radio, shut the window). You **CANNOT** fix this in post. 
 * test sound without your subject first, to make sure everything is properly configured, batteries are charged, levels are approximately right. 
 * record audio at the best quality you can, and at 48Khz, not 44.1.

**Once subject is seated:**

 * check/adjust furniture/background/objects 
 * check/adjust framing + focus
 * check/adjust lighting 
 * check sound levels and clarity. To work out the correct level for the interviewee's speaking voice, ask them to tell you 'what they had for breakfast' or something simple. 

**Situation-dependent:** 

 * makeup: shiny, sweaty interviewees look uncomfortable, and can make video look 'cheap' - a small amount of foundation will help a lot.

## The Interview

**Explain the process to Interviewee**

Giving them an understanding of what is going on often helps to put them at ease:

*"We'll be talking for about 15 minutes, then our team will pick the best parts and edit this down to 4 minutes or so.* 

*Please speak slowly and clearly, and try to answer in complete sentences where possible.* 

*We have plenty of time, and plenty of disk space - this is not live, so if you need a moment to formulate your answers, that's fine, we will cut out empty space in the edit.* 

*Don't worry about repeating yourself - sometimes it's useful for our edit to have the same idea formulated in different ways.* 

*If you want to start something again, or have me rephrase my question, that's totally okay.* 

*Please look at me while we're talking, rather than the camera, and if you need to stop for any reason, that's fine, just let me know.*

*OK, so we'll start with a few questions about you and your background, and then talk about your current project.”*

**During the interview:** 

 * Are you a two-person crew? Great! then one person can look after the technical aspects, and the interviewer can concentrate entirely on the conversation. You can check in with one another between questions.

 * If you're alone, then you should do one more quick check of all the technical aspects before you start shooting, and perform further checks *between* questions - don't be focused on the camera while your subject is trying to talk to you!

 * it's best not to interrupt the interviewee for minor issues. Most things can wait until they are finished answering a question. But if the problem is big enough that a whole section will be unusable, of course you should interrupt politely and stop your interviewee. 

 * If it's a technical issue (microphone noise, flat battery etc) try not to worry the interviewee about it - but let them know what's going on.
 
*"I'm sorry but we'll just have to pause there for a moment - you're doing great, but unfortunately your microphone has just slipped down and is creating some noise. We'll just quickly adjust it and then I'll ask the question once more."* 

 * if the problem *is* the interviewee, find a diplomatic way of offering constructive criticism.
*“this is excellent information, but it would be great if you could speak a little slower, and please try to avoid too many ums and ahhs if possible."*

**Listen to what they are saying:** 

 * if an opportunity presents itself to ask further questions, do so! 
Your prepared questions are just a guideline, and you can go off the script at any time if you feel it is interesting / important. 

**After your questions:**
 
 * there may be something that the interviewee wishes to discuss which you have not asked - ask them if there is anything else they feel would be useful to mention, or if there's anything else they'd like to add. 
 * did they mention anything in their answers which you may be able to get a shot of while you're there? It could be useful in the edit. **Take the opportunity** while you have it.
 * thank the interviewee!
