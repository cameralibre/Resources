# VOICEOVER RECORDING GUIDELINES
## Equipment needed:

- a quiet room in which to record
- a good quality microphone (Ask your friends/your network if anyone has gear you can borrow)
- a recording device (this may be an all-in-one device like a Zoom or TASCAM recorder, or it may be a laptop with [Audacity](http://audacityteam.org) installed, for example)
- any cables needed for the gear to work together (eg XLR cable)
- a microphone stand or other method of attaching the microphone in front of the speaker
- headphones to monitor the audio during recording 
- an SD card or whatever recording medium your device records to

#### OPTIONAL
- [pop shield](https://en.wikipedia.org/wiki/Pop_filter)
- a way of isolating the microphone from vibrations (eg. [shock mount](https://en.wikipedia.org/wiki/Shock_mount))
- a [pre-amplifier](https://en.wikipedia.org/wiki/Preamplifier) to raise the level of the recorded signal, and therefore increase the signal-to-noise ratio of the recording.


# SETUP 

## Recording room:

### isolation from external noises
- close any windows and doors
- check for, and switch off, any noise-producing devices (refrigerators, fans, radio)
- ask your neighbours/workmates to be quiet while you are recording

### echo reduction
- fill your recording room with **soft furnishings**. An empty room (or one with only flat, hard, reflective surfaces like tables) will reflect sound waves, and this reverberation will be picked up by the microphone - _this can't be removed in post-production_. If the room in which you plan to record does not normally contain many soft furnishings (curtains, soft chairs, etc), it is a good idea to bring in a couple of blankets, a few cushions, or a duvet/comforter to help with sound absorption.
- if you feel like it, build a fort from cushions to record in :)

![](https://media.giphy.com/media/OrB0Ruvuy7kIw/giphy.gif)


### angle of speaker & microphone. 
- align the direction of the speaker's voice and the microphone **at an angle** to the wall, so that they are not speaking directly towards the wall, or directly towards a corner. (see diagram)

![](PNG/reflection.png)

### reduction of handling sounds
- do not hold the microphone in your hands. Humans are good at many things, but no good at being microphone stands. Moving a microphone during recording will introduce unwanted noises. 
Use a proper microphone stand, or alternatively, find a way to attach the microphone to something which will not move during recording. (attach it to a lightstand using duct tape, or attach it to a chair, on top of a table). Vibrations from the floor can carry through a stand to the microphone, so nobody in the room should move around during recording.

## Speaker position

- what is most important - more important than any technical requirement - is that the person speaking feels comfortable. The choice to sit or stand should be their decision. 
- a seated speaker will be easier to record, as they will not move much further away or closer to the microphone during recording. However, the position of their body means that their diaphragm may be restricted, which can have an effect on how the voice sounds.
- a standing speaker is able to speak more naturally, but they may move around more during recording. Once everything is set up for recording, make marks on the floor with tape so that they know where to put their feet - this will ensure that recordings are consistent. Make sure that their feet are correctly placed before every new recording.

## Mic position:

- set up the microphone so it is about 15-20cm from the speaker's mouth
- if you have a 'pop shield', place this between the speaker's mouth and microphone. (if you want to make your own pop shield, you can use old pantyhose stretched over a frame of some kind)
- if you _do not_ have a pop shield, you can set up the microphone 'off axis' to the speaker's mouth, so that it does not receive the full force of the sound directly.
![](PNG/off-axis.png)

- set up the microphone at a height level to the speaker's mouth, and test it. If the "S" sounds are unnaturally "essssy" or distorted, it may help to lower the microphone slightly. If the "P" or "B" sounds are making loud impact sounds, it may help to raise the microphone slightly. If you can't really tell either way, then it is probably fine.

## Audio settings:

- Codec/File Type: **WAV/FLAC**
 - If your recording device can record WAV or FLAC, please use one of these - they are lossless codecs which give maximum flexibility in post-production, and no quality is lost through data compression. If your device can _only_ record in MP3 or another lossy format, it really is worth asking around to try to find a recorder which will record WAV or FLAC. MP3 would still work in an emergency, but it is not recommended...
- Bit Rate / Mbps: **Highest Possible (biggest number) eg. 256 Mbps**
 - this is a quality setting - set it to the maximum possible on your device.

- Frequency: **48KHz**
 - if 48KHz is not available, 44.1Khz will do.
 
- Lo-Cut: **OFF** 
 - any pre-processing (equalization / low-cut filter, compression, etc) should be kept to a minimum unless you're sure that know what you are doing. You cannot 'undo' this, so it is better to record without pre-processing, and then you will have flexibility in post-production later. An active Low Cut filter may be indicated with a symbol - something like ⧸▔▔ with a slope and then a flat line. 

- Audio Level **-12dB**
  - once you have your microphone and speaker set up, and your settings configured, you can adjust the recording volume on your device. You will see the audio monitor as a histogram, one or two bars indicating volume, which will move up & down as the speaker talks. There will probably be a scale shown on the side, with **0 db** at the top, and negative dB values going down. 
  - Ask the speaker to practise what they will record, at their natural volume level. 
  If the audio level is too loud, the audio will sound distorted and the bars indicating audio volume will exceed the maximum. Depending on your device, a red light may show, or the bars themselves may turn red. _This is bad_. Your recording should not exceed 0dB at any time. You need to turn the audio input down. You will need to reduce the **input volume** of your device, until these bars are regularly peaking around **-12dB**. This will ensure that there is a little bit of room to allow for natural differences in volume as your speaker talks - they can be a little louder, or a little quieter, and there will still be a good signal. 
  - if the audio level is well below -12dB, you will need to increase the input volume - if the recording level is too low, the level of noise to signal will be too high, and it will not sound good.
  - Note - your device may also have an **output volume** which determines the loudness of the signal _in your headphones_ - you can adjust this for your comfort, but this doesn't affect the level of recording - make sure that it is the **input volume** you are adjusting. To be sure that it is **input** you're adjusting, turn it all the way up to see the bars peaking, and all the way down to see them drop right down. Then adjust it so that the audio level is reaching around -12dB most of the time that the speaker is talking. 