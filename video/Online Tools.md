# Online Tools for Video

Useful tools from around the web which can complement your video production workflow.

**[OTranscribe](http://otranscribe.com/)**
A simple but very effective tool to transcribe interviews before editing, or transcribe a completed video for subtitling. You can use the web app or just run it on your machine - instructions on [Github](https://github.com/oTranscribe/oTranscribe).

**[Amara](http://amara.org/)**
*"Amara is home to an award winning subtitle editor that makes it easy to caption and translate video. Amara also hosts volunteer localization & accessibility communities, and offers professional tools and services for subtitles."*
Amara actually has a really good subtitle editor tool, with the added addition of the possibility to collaborate with others. We used Amara to produce and translate the [POC21 documentary subtitles](http://amara.org/en/videos/JeK73mHQbIR4/info/proof-of-concept-100-geeks-5-weeks-1-future).

 **[SMPTE Generator](http://elteesee.pehrhovey.net/ltc/create)**
Needing to 'stripe' a video and its audio with [SMPTE Linear Timecode](https://en.wikipedia.org/wiki/Linear_timecode) is perhaps less common these days, so many F/LOSS editors don't come with a generator built-in. This will come in handy when you need it!

**[Timecode Calculator](http://www.1728.org/angle.htm)**
Especially useful if you're doing a bunch of chopping and changing with FFmpeg
.


