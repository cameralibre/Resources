### Monospaced


for use in the command line or text editors, where precision and equal spacing is important.


**[Courier Prime Code](http://quoteunquoteapps.com/courierprime/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), designed by Alan Dague-Greene for John August and Quote-Unquote Apps.*

Code-optimized Sans for programmers. Courier Prime Code features larger line height, new asterisk, slashed zero and straight-legged italic “f.”


**[Fira Mono](http://mozilla.github.io/Fira/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Digitized data © 2012-2015, The Mozilla Foundation and Telefonica S.A. with Reserved Font Name "Fira"* 

Mozilla's font, originally designed for the Firefox OS project

**[Lekton](https://www.google.com/fonts/specimen/Lekton)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2008-2010, [Isia Urbino](http://www.isiaurbino.net)*

Lekton has been designed at ISIA Urbino, Italy, and is inspired by some of the typefaces used on the Olivetti typewriters.
The typeface has been initially designed at ISIA Urbino by the students Luna Castroni, Stefano Faoro, Emilio Macchia, Elena Papassissa, Michela Povoleri, Tobias Seemiller, and the teacher Luciano Perondi (aka galacticus ineffabilis).

This typeface has been designed in 8 hours, and was inspired by some of the typefaces used on the Olivetti typewriters.

The glyphs are 'trispaced.' It means that the space are modular, 250, 500, 750, this allow a better spacing between characters, but allow also a vertical alignment similar to the one possible with a monospaced font. We were thinking it was a bright new idea, but we discovered that was usual for Olivetti typewriters working with 'Margherita.'

**[Source Code Pro](http://adobe-fonts.github.io/source-code-pro/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, 2012 [Adobe Systems Incorporated](http://www.adobe.com/), with Reserved Font Name 'Source'. All Rights Reserved. Source is a trademark of Adobe Systems Incorporated in the United States and/or other countries.*
