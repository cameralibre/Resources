### Blackletter


Gothic-style fonts, some more traditional, others more readable:


**[Blocus](http://www.velvetyne.fr/fonts/blocus/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Martin Desinde*

Blocus is a light typography, somewhere between a Fraktur and a Didone, with broken curves, a strong contrast between thick and thin lines and imposing vertical.

**[eufm10](http://www.ams.org/publications/authors/tex/amsfonts)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 1997, 2009, [American Mathematical Society](http://www.ams.org)*

**[Grobe Deutschmeister](http://www.peter-wiegel.de/GrobeDeutschmeister.html)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2014, [Peter Wiegel](www.peter-wiegel.de), wiegel@peter-wiegel.de with Reserved Font Name Grobe Deutschmeister*

Die Grobe Deutschmeister ist eine Frakturschrift im Stil der Neogotischen Schriften, wenn auch nicht mit der Strenge anderer Vertreter dieser in den 1920er Jahren aufgekom- menen und dem damaligen Zeitgeschmack entsprechen- den Art.

**[JustLetters](https://fontlibrary.org/en/font/just-letters)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Christopher Adams*

After Albrecht Dürer, Of the Just Shaping of Letters, 1525 (1917).

