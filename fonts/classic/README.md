## Classic

Historic typefaces! Some font files were released as public domain by the original designers (Chicago), but most are Libre recreations of typefaces that are either officially, or *should be* in the public domain. eg. [Eric Gill](https://en.wikipedia.org/wiki/Eric_Gill) died in 1940, so his work should have become public domain in Europe in 2010. But a font is not just the drawings, it's also the software and automatic kerning etc, so that's what Monotype claims under exclusive © for Gill Sans. 'Sans Guilt' was recreated by tracing Eric Gill's early sketches. Futura Renner is a similar story.
Courier Prime is a modern update, to make scriptwriters' lives a litte prettier.


**[ChicagoFLF](http://christtrekker.users.sourceforge.net/fnt/chicago.shtml)**
*Public Domain*

Robin Casady created ChicagoFLF and placed it in the public domain. This version contains the following changes from the original: *Moved symbols to correct Unicode positions. *Incorporated additional symbols from CheckboxFLF, which used identical Basic Latin glyphs, and thus was essentially the "same" font with different symbols.


**[Courier Prime](http://quoteunquoteapps.com/courierprime/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), designed by Alan Dague-Greene for John August and Quote-Unquote Apps.*

Since the beginning, screenplays have been written in Courier. Its uniformity allows filmmakers to make handy comparisons and estimates, such as 1 page = 1 minute of screen time.
But there’s no reason Courier has to look terrible. We set out to make the best damn Courier ever.


**[DIN OSP](http://ospublish.constantvzw.org/foundry/osp-din/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011 [OSP](http://ospublish.constantvzw.org).*

The first cut of OSP-DIN was drawn for the festival Cinema du réel 2009, when we were invited to work on a cartographic version of the programme (see "OSP_cine-du-reel2009.jpg" in the documentation folder). We drew the first cut of the open source DIN from grid based drawings similar to the original 1932 drawings of the DIN we saw during our investigative trip to Berlin in february 2008.

**[Futura Renner](https://github.com/uplaod/FuturaRenner)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Bastien Sozoo*

Futura Renner family is a digital version of Futura lead character belonging to the typography department of Ecole Nationale Supérieure des Arts de la Cambre, and probably the first draft of Futura as we know it.

**[Goudy Bookletter 1911](https://www.theleagueofmoveabletype.com/goudy-bookletter-1911)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009, Barry Schwartz site-chieftain@crudfactory.com, with Reserved Font Name OFL "Goudy Bookletter 1911".*
Based on Frederic Goudy’s Kennerley Oldstyle.

A few words on why I think Kennerley Oldstyle is beautiful: In making this font, I discovered that Kennerley fits together tightly and evenly with almost no kerning. Thus the following words from Monotype specimen books are just: “[W]hen composed into words the characters appear to lock into one another with a closeness common in early types, but not so often seen in later-day creations.” These are letters that take command of the space around them; notice, for instance, the bowed shapes of the v and w.

**[Libre Baskerville](http://www.impallari.com/projects/overview/libre-baskerville)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Libre Baskerville is webfont optimized for web body text (typically 16px). It's based on 1941 ATF Specimens, but it has a taller x height, wider counters and minor contrast that allow it to work on small sizes in any screen.


**[Libre Bodoni](https://github.com/impallari/Libre-Bodoni)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

The Libre Bodoni fonts are based on the 19th century Morris Fuller Benton's ATF design, but specifically adapted for today's web requirements.

They are a perfect choice for everything related to elegance, style, luxury and fashion.


**[Libre Franklin](https://github.com/impallari/Libre-Franklin)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Libre Franklin is an interpretation and expancion based on the 1912 Morris Fuller Benton’s classic.


**[OFL Sorts Mill Goudy](https://www.theleagueofmoveabletype.com/sorts-mill-goudy)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009, [Barry Schwartz](site-chieftain@crudfactory.com), with Reserved Font Name OFL "Sorts Mill Goudy".*

A 'revival' of Goudy Oldstyle and Italic, with features including small capitals (in the roman only), oldstyle and lining figures, superscripts and subscripts, fractions, ligatures, class-based kerning, case-sensitive forms, and capital spacing. There is support for many languages using latin scripts.


**[Sans Guilt](http://ospublish.constantvzw.org/foundry/sans-guilt/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011 OSP* 

Versions of Gill Sans based on three different sources. 
Sans Guilt MB: Based on a rasterized pdf made with the Monotype Gill Sans delivered with Mac OSX. 
Sans Guilt DB: Based on early sketches by Eric Gill. 
Sans Guilt LB: Based on lead type from Royal College of Arts letterpress workshop.
