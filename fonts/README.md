Here's a collection of the most useful and interesting Libre typefaces I have found.
To discover more, explore the [Font Library](https://fontlibrary.org) or check out the work of [League of Moveable Type](https://www.theleagueofmoveabletype.com), read issue 2.3 of [Libre Graphics Magazine](http://libregraphicsmag.com/), browse through the typographic adventures of [Open Source Publishing](http://osp.kitchen/), the [collection](http://usemodify.com/) of Raphaël Bastide or [Open Foundry](http://open-foundry.com/).

To install these fonts in an Ubuntu system, just download this folder or individual folders as zip (or [clone](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository) the repository) and extract the .otf or .ttf files into your [hidden](http://www.ghacks.net/2009/04/16/linux-tips-view-hidden-files/) .fonts folder in your home directory. (you may need to create a new folder called .fonts if it doesn't exist yet)



### Blackletter


Gothic-style fonts, some more traditional, others more readable:


**[Blocus](http://www.velvetyne.fr/fonts/blocus/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Martin Desinde*

Blocus is a light typography, somewhere between a Fraktur and a Didone, with broken curves, a strong contrast between thick and thin lines and imposing vertical.

**[eufm10](http://www.ams.org/publications/authors/tex/amsfonts)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 1997, 2009, [American Mathematical Society](http://www.ams.org)*

**[Grobe Deutschmeister](http://www.peter-wiegel.de/GrobeDeutschmeister.html)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2014, [Peter Wiegel](www.peter-wiegel.de), wiegel@peter-wiegel.de with Reserved Font Name Grobe Deutschmeister*

Die Grobe Deutschmeister ist eine Frakturschrift im Stil der Neogotischen Schriften, wenn auch nicht mit der Strenge anderer Vertreter dieser in den 1920er Jahren aufgekom- menen und dem damaligen Zeitgeschmack entsprechen- den Art.

**[JustLetters](https://fontlibrary.org/en/font/just-letters)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Christopher Adams*

After Albrecht Dürer, Of the Just Shaping of Letters, 1525 (1917).


### Body / Multipurpose


Useful Sans or Serif styles, in multiple weights, including italics wherever possible:


**[Aileron](http://dotcolon.net/font/aileron/)**
*[CC0 (No Rights Reserved)](https://creativecommons.org/choose/zero/)m By Sora Sagano*

Aileron is a typeface that is classified as neo-grotesque, including Helvetica as a reference, is a sans-serif font that summarizes add the interpretation of my own.

The spread between the slightly shaped as an adjustment to the body for, in order to facilitate the identification of the case-eye "I", we have to design that curve the distal portion of the lower case El "l".

In addition, the dot part of, such as "i" and "j" or period in a circle, by performing a process with an awareness of clothoid curve to curve part, was to get an overall soft impression. The design basis is close to Helvetica, but conceptually it has become rather close to Univers.

In addition, we are for the first time provide a italic as the font of this dot colon. Basic is what tilted mechanically, but has undergone a visual correction so that there is no sense of incongruity.

Taking advantage of the multiple master, weights are available 8 stage from UltraLight to Black. I think that you can use in a wide scene.

Please read about the dot colon fonts when you use the font.

**[Aleo](https://www.behance.net/gallery/ALEO-Free-Font-Family/8018673)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Alessio Laiso*

Aleo is a contemporary typeface designed by Alessio Laiso as the slab serif companion to the Lato font by Łukasz Dziedzic. Aleo has semi-rounded details and a sleek structure, giving it a strong personality while still keeping readability high. The family comprises six styles: three weights (light, regular and bold) with corresponding true italics.

**[Arvo](https://www.google.com/fonts/specimen/Arvo)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Anton Koovit*

 Arvo is a geometric slab-serif typeface family suited for screen and print. The flavour of the font is rather mixed. Its monolinear-ish, but has a tiny bit of contrast (which increases the legibility a little in Mac OS X.)

The name Arvo is a typical Estonian man's name, but is not widely used today. In the Finnish language, Arvo means "number, value, worth." Considering how much programming is involved in hinting, all these meanings are true. 


**[Belgika](http://osp.kitchen/foundry/belgica-belgika/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), OSP / Pierre Huyghebaert](http://www.speculoos.com), pierre@speculoos.com*

A majuscule sans-serif font from OSP with uniform stroke widths, based on sundry generic sources.

**[Bellota](http://www.pixilate.com/font/bellota/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Kemie Guaida*

Based on the font “Snippet” by Gesine Todt, Bellota is an ornamented, low contrast sans-serif. It’s just cute enough!

**[Cantarell](https://git.gnome.org/browse/cantarell-fonts/tree/README)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009--2016, [The Cantarell Authors](https://git.gnome.org/browse/cantarell-fonts/tree/CONTRIBUTORS)*

The Cantarell typeface family is a contemporary Humanist 
sans serif, and is used by the GNOME project for its user
interface and the Fedora project. 

**[Cormorant](https://www.behance.net/gallery/28579883/Cormorant-an-open-source-display-font-family)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Christian Thalmann, [Catharsis Fonts](https://www.myfonts.com/foundry/Catharsis_Fonts)*

Cormorant is an original design for an extravagant display serif typeface inspired by the Garamond heritage, hand-drawn and produced by Catharsis Fonts. While traditional Garamond cuts make for exquisite reading at book sizes, they appear clumpy and inelegant at larger sizes. The design goal of Cormorant was to distill the æsthetic essence of Garamond, unfetter it from the limitations of metal printing, and allow it to bloom into its natural refined form at high definition.

Cormorant is characterized by scandalously small counters, razor-sharp serifs, dangerously smooth curves, and flamboyantly tall accents. While many implementations of Garamond at small optical sizes already exist (including the open-sourced EB Garamond by Georg Duffner), Cormorant aims for the sparsely populated niche of display-size counterparts that exploit the high resolution of contemporary screens and print media to the fullest.


**[Dosis](http://www.impallari.com/dosis)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Dosis is a rounded sans-serif type family. It started with the Extra Light style, useful only at size 36pt or upm and the Extended Latin character set included many alternative characters, all designed by Edgar Tolentino and Pablo Impallari.


**[Encode](http://www.impallari.com/projects/overview/encode)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Some letters have abrupt asymmetrical curves, while other are soft and fluid. This contrasting combination allows for a pleasant modern feeling, without being squarish, boring or repetitive.
What started as a single style font, is now becoming a 45 styles super-family.


**[Fira Sans](http://mozilla.github.io/Fira/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Digitized data © 2012-2015, The Mozilla Foundation and Telefonica S.A. with Reserved Font Name "Fira" * 

Mozilla's font, originally designed for the Firefox OS project

**[Gentium](http://software.sil.org/gentium/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2003-2014 SIL International (http://www.sil.org/), with Reserved Font Names "Gentium" and "SIL".*

Gentium is a typeface family designed to enable the diverse ethnic groups around the world who use the Latin, Cyrillic and Greek scripts to produce readable, high-quality publications. It supports a wide range of Latin- and Cyrillic-based alphabets. [Gentium on Wikipedia](https://en.wikipedia.org/wiki/Gentium).

**[Hanken](https://fontlibrary.org/en/font/hanken)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015, [Alfredo Marco Pradil](http://behance.net/pradil), ammpradil@gmail.com, with Reserved Font Name Hanken.*

Geometric and rounded font.

**[HK Grotesk](https://hanken.co/product/hk-grotesk/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015, [Alfredo Marco Pradil](http://behance.net/pradil), ammpradil@gmail.com, with Reserved Font Name HK Grotesk.*

HK Grotesk is a sans serif typeface inspired from the classical grotesques. The goal in designing HK Grotesk is to create a more friendly and distinguishable typeface that is suitable for small text.

**[Josefin Sans](https://www.google.com/fonts/specimen/Josefin+Sans)**
*[SIL Open Font License](http://scripts.sil.org/OFL), [Santiago Orozco](http://typemade.mx/)*

The idea for creating this typeface was to make it geometric, elegant and kind of vintage, especially for titling. It is inspired by Rudolf Koch's Kabel (1927), Rudolf Wolf's Memphis (1930), Paul Renner's Futura (1927).
My idea was to draw something with good style, that reflects Swedish design and their passion for a good lifestyle, and by default all other Scandinavian styles.
Something weird happened when I drew the letter "z": when I took my typography courses in college, I saw a very interesting typeface in a book, the New Universal Typeface "Newut" from André Baldinger (which I have loved since then), and after I finished the "z" I ran into Newut' site again and noticed that I had unconsciously drawn it with the same haircut.
The x-height is half way from baseline to caps height, unlike any other modern typeface.
I wanted to do something different with the ampersand, so I made three and will include them as alternates in future versions. This version also includes Old Style Numerals.

**[Josefin Slab](https://www.google.com/fonts/specimen/Josefin+Slab)**
*[SIL Open Font License](http://scripts.sil.org/OFL), [Santiago Orozco](http://typemade.mx/)*

Josefin Slab was the first typeface–at least in my mind–I designed! But I decided to start simple with Josefin Sans. Following the 1930s trend for geometric typefaces, it just came to me that something between Kabel and Memphis with modern details will look great.

I wanted to stick to the idea of Scandinavian style, so I put a lot of attention to the diacritics, especially to "æ" which has loops connecting in a continuous way, so the "e" slope was determined by this character.

It also has some typewriter style attributes, because I've liked the Letter Gothic typeface since I was in high school, and that's why I decided to make a Slab version of Josefin Sans.

**[Lato](http://www.latofonts.com/lato-free-fonts)**
*[SIL Open Font License](http://scripts.sil.org/OFL), [Łukasz Dziedzic](http://www.latofonts.com/team/)*

Lato is a sanserif type­face fam­ily designed in the Sum­mer 2010 by Warsaw-​​based designer Łukasz Dziedzic (“Lato” means “Sum­mer” in Pol­ish). In Decem­ber 2010 the Lato fam­ily was pub­lished under the open-​​source Open Font License by his foundry tyPoland, with sup­port from Google.

In the last ten or so years, during which Łukasz has been designing type, most of his projects were rooted in a particular design task that he needed to solve. With Lato, it was no different. Originally, the family was conceived as a set of corporate fonts for a large client — who in the end decided to go in different stylistic direction, so the family became available for a public release.

When working on Lato, Łukasz tried to carefully balance some potentially conflicting priorities. He wanted to create a typeface that would seem quite “transparent” when used in body text but would display some original traits when used in larger sizes. He used classical proportions (particularly visible in the uppercase) to give the letterforms familiar harmony and elegance. At the same time, he created a sleek sans serif look, which makes evident the fact that Lato was designed in 2010 — even though it does not follow any current trend.

The semi-rounded details of the letters give Lato a feeling of warmth, while the strong structure provides stability and seriousness. “Male and female, serious but friendly. With the feeling of the Summer,” says Łukasz.

**[Linux Libertine](http://www.linuxlibertine.org)**
*[GNU GPL General Public License version 2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) (GPLv2), [SIL Open Font License](http://scripts.sil.org/OFL)*

We work on a versatile font family. It is designed to give you an alternative for fonts like T*mes New Roman. 


**[Lora](http://www.cyreal.org/2012/07/lora/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011 Olga Karpushina*

Lora is a well-balanced contemporary serif with roots in calligraphy. It is a text typeface with moderate contrast well suited for body text.

A paragraph set in Lora will make a memorable appearance because of its brushed curves in contrast with driving serifs. The overall typographic voice of Lora perfectly conveys the mood of a modern-day story, or an art essay.


**[Montserrat](https://www.google.com/fonts/specimen/Montserrat)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012 Julieta Ulanovsky*

The old posters and signs in the traditional neighborhood of Buenos Aires called Montserrat inspired me to design a typeface that rescues the beauty of urban typography from the first half of the twentieth century. The goal is to rescue what is in Montserrat and set it free, under a free, libre and open source license, the [SIL Open Font License](http://scripts.sil.org/OFL).

As urban development changes this place, it will never return to its original form and loses forever the designs that are so special and unique. To draw the letters, I rely on examples of lettering in the urban space. Each selected example produces its own variants in length, width and height proportions, each adding to the Montserrat family. The old typographies and canopies are irretrievable when they are replaced.

There are other revivals, but those do not stay close to the originals. The letters that inspired this project have work, dedication, care, color, contrast, light and life, day and night! These are the types that make the city look so beautiful.

**[PropCourierSans](http://manufacturaindependente.org/propcourier-sans/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2013 Manufactura Independente*

PropCourier is the canonical typeface for Libre Graphics magazine. It was created through a conversion of OSP's NotCourier Sans from a monospaced font into a proportional one.

From there, we've been refining the design and releasing new versions with each issue of Libre Graphics magazine, until we settled on a final design when we released issue 2.2 of the magazine.

PropCourier-Sans is a fork of Open Source Publishing’s [Not-Courier Sans](http://osp.kitchen/foundry/notcouriersans/), using proportional spacing and some finer tweaks. 12 Official typeface for the Libre Graphics Magazine. PropCourier-Sans is available in three weights: Regular, Medium and Bold.
NotCourier was designed by OSP (Ludivine Loiseau), in July 2008. It is based on Nimbus Mono, © 1999 by (URW)++ Design & Development; Cyrillic glyphs added by Valek Filippov (C) 2001-2005.

**[Open Sans](https://www.google.com/fonts/specimen/Open+Sans)**
*[Apache License](http://www.apache.org/licenses/LICENSE-2.0.html), Steve Matheson*

Open Sans is a humanist sans serif typeface designed by Steve Matteson, Type Director of Ascender Corp. This version contains the complete 897 character set, which includes the standard ISO Latin 1, Latin CE, Greek and Cyrillic character sets. Open Sans was designed with an upright stress, open forms and a neutral, yet friendly appearance. It was optimized for print, web, and mobile interfaces, and has excellent legibility characteristics in its letterforms.

**[Overpass](http://overpassfont.org/)**
*Dual Licensed under the [SIL Open Font License](http://scripts.sil.org/OFL) and also the [LGPL 2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html), © 2015 Red Hat, Inc.*

Overpass is the typeface used by Red Hat. The origin-artwork that inspired Overpass (Highway Gothic) is public domain.
Used globally in road signage, Highway Gothic embodies design for the public good.
Traffic control systems demonstrate that open standards and open communication help complex communities work better together.
This digital interpretation of Highway Gothic includes a wide array of variations so that web designers have the flexibility they expect for modern web typography.
Today’s enterprise brands all have distinct typographic expressions. In the software arena, Overpass is strongly aligned to Red Hat brand.

**[PT Sans](https://www.google.com/fonts/specimen/PT+Sans)**
*[SIL Open Font License](http://scripts.sil.org/OFL) © 2010, [ParaType Ltd.](http://www.paratype.com/public), with Reserved Font Names "PT Sans" and "ParaType".*

PT Sans was developed for the project "Public Types of Russian Federation." The main aim of the project is to give possibility to the people of Russia to read and write in their native languages.

The project is dedicated to the 300 year anniversary of the civil type invented by Peter the Great in 1708–1710. It was given financial support from the Russian Federal Agency for Press and Mass Communications.

The fonts include standard Western, Central European and Cyrillic code pages, plus the characters of every title language in the Russian Federation. This makes them a unique and very important tool for modern digital communications.

PT Sans is based on Russian sans serif types of the second part of the 20th century, but at the same time has distinctive features of contemporary humanistic designs.


**[PT Serif](https://www.google.com/fonts/specimen/PT+Serif)**
*[SIL Open Font License](http://scripts.sil.org/OFL) © 2010, [ParaType Ltd.](http://www.paratype.com/public), with Reserved Font Names "PT Sans" and "ParaType".*

PT Serif™ is the second pan-Cyrillic font family developed for the project “Public Types of the Russian Federation.”


**[Raleway](https://www.theleagueofmoveabletype.com/raleway)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, Matt McInerney matt@pixelspread.com, with Reserved Font Name: "Raleway".*

Raleway is an elegant sans-serif typeface, designed in a single thin weight. It is a display face that features both old style and lining numerals, standard and discretionary ligatures, a pretty complete set of diacritics, as well as a stylistic alternate inspired by more geometric sans-serif typefaces than it's neo-grotesque inspired default character set.


**[Rubik](http://hubertfischer.com/work/type-rubik)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015 by Hubert & Fischer. 

Google Creative Lab approached us to design a typeface for the branding of the Rubik's Cube Exhibition ”Beyond Rubik's Cube“ at the Liberty Science Center, Jersey City. We designed a slightly rounded heavyweight font in which the letters fit perfectly in a single cubelet of the Rubik's Cube. The font was expanded to include Cyrillic and Hebrew characters for the exhibition, which will travel internationally.


**[Source Sans Pro](http://adobe-fonts.github.io/source-sans-pro/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, 2012 [Adobe Systems Incorporated](http://www.adobe.com/), with Reserved Font Name 'Source'. All Rights Reserved. Source is a trademark of Adobe Systems Incorporated in the United States and/or other countries.*

Sans serif font family for user interface environments. 


**[Source Serif Pro](http://adobe-fonts.github.io/source-serif-pro/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, 2012 [Adobe Systems Incorporated](http://www.adobe.com/), with Reserved Font Name 'Source'. All Rights Reserved. Source is a trademark of Adobe Systems Incorporated in the United States and/or other countries.*

Serif typeface designed to complement Source Sans Pro. 


**[Yanone Kaffesatz](http://www.yanone.de/fonts/kaffeesatz/)**
*[CC BY 2.0 DE](https://creativecommons.org/licenses/by/2.0/de/) Jan Gerner*

Yanone Kaffeesatz was first published in 2004 and is Yanone’s first ever finished typeface. Its Bold is reminiscent of 1920s coffee house typography, while the rather thin fonts bridge the gap to present times. Lacking self confidence and knowledge about the type scene, Yanone decided to publish the family for free under a Creative Commons License. A decision that should turn out one of the best he ever made. It has been downloaded over 100,000 times to date, and you can witness Kaffeesatz use on German fresh-water gyms, Dubai mall promos and New Zealand McDonalds ads. And of course on coffee and foodstuff packaging and café design around the globe



### Modular

made of modular components or designed to fit with modular systems


**[Amalgame](https://fontlibrary.org/en/font/amalgame)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Félicie Bouckaert*

A modular typeface composed with only 11 different shapes.


**[Miliimetre](http://velvetyne.fr/fonts/millimetre/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Jérémy Landes-Nones*

Millimetre is a series of fonts constructed on a grid based on the metric system. It follows the decimal logic of the latter. In this spirit, when you typeset Millimetre, please don’t use the archaic unit of the point but the millimetre, centimetre, decimeter or the meter itself for the really big sizes.

In this typeface, each em-square is vertically and horizontally divided in 10 units (decimal, remember?). Printed at a 1 cm size, the strokes of the regular weight will be 1 mm thick. Both white spaces and black stems fit on this grid. Half of the lines and columns of this 10x10 grid receive the stems and the strokes of this font whereas the other half is there to receive the white spaces inside the letters and between them, making millimetre rythm quite unique, totally settled, like a barcode. To make it clearer, when you typeset two m lowercases, the thickness of the stems of the m will be equal to the counters between its legs, to the thins and to the space between the two letters. This grid-based design, aligned to a pixel grid, makes Millimetre works quite well on screen too. When typesetted with a leading equal to its size, the grid appears in the perfect alignment of the stems between the different lines of text. No corrections needed.



### Monospaced


for use in the command line or text editors, where precision and equal spacing is important.


**[Courier Prime Code](http://quoteunquoteapps.com/courierprime/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), designed by Alan Dague-Greene for John August and Quote-Unquote Apps.*

Code-optimized Sans for programmers. Courier Prime Code features larger line height, new asterisk, slashed zero and straight-legged italic “f.”


**[Fira Mono](http://mozilla.github.io/Fira/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Digitized data © 2012-2015, The Mozilla Foundation and Telefonica S.A. with Reserved Font Name "Fira"* 

Mozilla's font, originally designed for the Firefox OS project

**[Lekton](https://www.google.com/fonts/specimen/Lekton)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2008-2010, [Isia Urbino](http://www.isiaurbino.net)*

Lekton has been designed at ISIA Urbino, Italy, and is inspired by some of the typefaces used on the Olivetti typewriters.
The typeface has been initially designed at ISIA Urbino by the students Luna Castroni, Stefano Faoro, Emilio Macchia, Elena Papassissa, Michela Povoleri, Tobias Seemiller, and the teacher Luciano Perondi (aka galacticus ineffabilis).

This typeface has been designed in 8 hours, and was inspired by some of the typefaces used on the Olivetti typewriters.

The glyphs are 'trispaced.' It means that the space are modular, 250, 500, 750, this allow a better spacing between characters, but allow also a vertical alignment similar to the one possible with a monospaced font. We were thinking it was a bright new idea, but we discovered that was usual for Olivetti typewriters working with 'Margherita.'

**[Source Code Pro](http://adobe-fonts.github.io/source-code-pro/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, 2012 [Adobe Systems Incorporated](http://www.adobe.com/), with Reserved Font Name 'Source'. All Rights Reserved. Source is a trademark of Adobe Systems Incorporated in the United States and/or other countries.*


### Title/Fun/Curious


Some are 'normal' fonts that only come in one weight, some have interesting stories behind them (eg Engineer Hand, Mill, Nanook, Serriera) and the 'fun' fonts are generally at the more useful end of the ¡FUN FONTS! spectrum.


**[Acidente](https://gitlab.com/manufacturaind/acidente)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2013, [Luís Camanho](luiscamanho@gmail.com), with Reserved Font Name 'Acidente'.*

Acidente was drafted in paper in 2009, to be used in a set of poster designs. The shapes are inspired by Akzidenz Grotesk.

In 2013 it was digitised, using Insckape and FontForge.


**[Antonio](https://www.fontsquirrel.com/fonts/antonio)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011-12, [vernon adams](vern@newtypography.co.uk), with Reserved Font Names ‘Antonio’*

Antonio is a reworking of a traditional advertising sans serif typeface. The letter forms have been digitised and then reshaped for use as a webfont, the counters have been opened up a little and the stems optimised for use as bold display font in modern web browsers.

**[Antwerp](http://www.citype.net/city/antwerp)**
*[Creative Commons - Attribution](https://creativecommons.org/licenses/by/3.0/) (CC BY 3.0) [Thijs Meulendijks](http://www.thijsmeulendijks.nl/)*

[Citype](http://www.citype.net/) is a project where designers create free typefaces inspired by a city. The typeface depicts the different cultures living together but apart in Antwerp. Its vertical appearance refers to the sometimes narrow-minded people and closed nature of people.
For some reason, the font file is called '2602.otf' and fails to install properly when renamed 'Antwerp.otf'. I don't know enough about fonts to fix this. A little help?


**[Arca Majora](https://hanken.co/product/arca-majora-2/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015, [Alfredo Marco Pradil](http://behance.net/pradil) (ammpradil@gmail.com), with Reserved Font Name ARCA MAJORA.*

Arca Majora is an all-uppercase geometric sans-serif that is great for headlines, logotypes and posters. Sharp tips and bold stems—Arca Majora is perfect for high-impact communication.


**[Beon](https://github.com/uplaod/Beon)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Bastien Sozoo*

A neon font


**[Engineer Hand](https://fontlibrary.org/en/font/engineer-hand)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Nick Matavka*

Architects and engineers use a particular, very regimented script for formal drawings. This is for legibility purposes and is known as Draftsman's Hand, Engineer's Hand, or Architect's Hand. The same script is often used in pre-1950's book illustrations, when they were all done by hand. This font attempts to replicate one such example, from an American War Department collection of ammunition drawings. The letter sizes are a bit irregular, but this font is still an excellent choice to label diagrams and technical blueprints for that hand-drawn, 1950's look.

**[Fachada](https://gitlab.com/manufacturaind/fachada)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2013, [Rui Silva](ruisilva@alfaiataria.org), with Reserved Font Name 'Fachada'.*

**[League Gothic](https://www.theleagueofmoveabletype.com/league-gothic)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, [Caroline Hadilaksono](caroline@hadilaksono.com) & [Micah Rich](micah@micahrich.com), with Reserved Font Name: "League Gothic".*

League Gothic is a revival of an old classic, and one of our favorite typefaces, Alternate Gothic #1. It was originally designed by Morris Fuller Benton for the American Type Founders Company in 1903. The company went bankrupt in 1993, and since the original typeface was created before 1923, the typeface is in the public domain.

We decided to make our own version, and contribute it to the Open Source Type Movement. Thanks to a commission from the fine & patient folks over at WND.com, it's been revised & updated with contributions from Micah Rich, Tyler Finck, and Dannci, who contributing extra glyphs.

**[League Spartan](https://www.theleagueofmoveabletype.com/league-spartan)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © September 22 2014, [Micah Rich](micah@micahrich.com), with Reserved Font Name: "League Spartan".*

A new classic, this is a bold, modern, geometric sans-serif that has no problem kicking its enemies in the chest.

Taking a strong influence from ATF's classic Spartan family, we're starting our own family out with a single strong weight. We've put a few unique touches into a beautiful, historical typeface, and made sure to include an extensive characterset – currently totaling over 300 glyphs.

**[LilGrotesk](https://github.com/uplaod/LilGrotesk)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Bastien Sozoo*

Lil Grotesk is a sans-serif typeface in two weights.

**[Matchbook](http://www.onebyfourstudio.com/projects/fonts/2009/matchbook-typefaces/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009, Brian Haines, [One by Four](info@onebyfourstudio.com)*

Matchbook is a simple and functional set of two typefaces we designed in a serif and sans-serif version. Each set includes all accented characters and works beautifully in larger scales.


**[Milano](http://www.citype.net/city/milano)**
*[Creative Commons - Attribution](https://creativecommons.org/licenses/by/3.0/) (CC BY 3.0) [Marco Oggian](http://www.citype.net/city/www.marcoggian.info)*

[Citype](http://www.citype.net/) is a project where designers create free typefaces inspired by a city. 
Milano is a noisy and chaotic city, with a little paradise area called Navigli. Navigli are little channels in the center of Milano, where you can chill yourself and drinking a beer seeing the beauty of the water. I love water.

**[Mill](http://osp.kitchen/foundry/mill/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012 OSP* 

Mill has been created for engraving building instructions into the wood of a bench.

**[Nanook](https://fontlibrary.org/en/font/nanook)**
*[SIL Open Font License](http://scripts.sil.org/OFL),  Lucas Le Bihan*

Based on the intertitle typeface in Robert Flaherty's early documentaries, like the groundbreaking [Nanook of the North](https://en.wikipedia.org/wiki/Nanook_of_the_North)
[PDF](https://github.com/LucasLeBihan/Nanook/blob/master/documentation/PRESENTATION.pdf)

**[Ostrich Sans](https://www.theleagueofmoveabletype.com/ostrich-sans)**
*[SIL Open Font License](http://scripts.sil.org/OFL),  © 2011, (Tyler Finck](hi@tylerfinck.com), with Reserved Font Name: "Ostrich Sans".*

A gorgeous modern sans-serif with a very long neck. With a whole slew of styles & weights:


**[Oswald](https://www.fontsquirrel.com/fonts/oswald)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012, [Vernon Adams](vern@newtypography.co.uk), with Reserved Font Names ‘Oswald’*

**[Pecita](http://pecita.eu/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © © 2009, [Philippe Cochy](http://pecita.com) with Reserved Font Name Pecita.*

Pecita is a typeface that mimics handwriting. The use of complex features makes this possible. The font is coded with full Unicode glyphs customary in Latin and pan European languages, the phonetic alphabet, katakana, mathematical operators and many symbols. 

**[Playfair Display](http://www.forthehearts.net/typeface-design/playfair-display/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Claus Eggers Sørensen*

Play­fair is a trans­itional design. From the time of enlight­en­ment in the late 18th cen­tury, the broad nib quills were replaced by poin­ted steel pens. This influ­enced typo­graph­ical let­ter­forms to become increas­ingly detached from the writ­ten ones. Devel­op­ments in print­ing tech­no­logy, ink and paper mak­ing, made it pos­sible to print let­ter­forms of high con­trast and del­ic­ate hairlines.

This design lends itself to this period, and while it is not a revival of any particular design, it takes influence from the designs of printer and typeface designer John Baskerville, the punchcutter William Martin’s typeface for the ‘Boydell Shakspeare’ (sic) edition, and from the ‘Scotch Roman’ designs that followed thereafter.


**[Pompiere](http://karolinalach.com/194342/2077414/work/pompiere-typeface)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Karolina Lach*

Pompiere is a narrow sans serif typeface that takes its inspiration from a handlettered sign found outside of a firehouse in New York City. It combines the qualities of delicate, handwritten craftsmanship with subtle Art Nouveau-inspired details and proportions. These characteristics combine to lend the typeface a friendly, unassuming atmosphere.


**[Serriera Sobria](https://gitlab.com/manufacturaind/serreria-sobria)**
 
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012, The participants of the "[Stone to Spaceship](http://manufacturaindependente.com/stonespaceship/)" workshop at Medialab Prado in July 2012, with Reserved Font Name 'Serreria Sobria'.*

A typeface designed collectively during the From Stone to Spaceship workshop, for Medialab Prado.
The soon to be home of Medialab-Prado, Serrería Belga (Old Belgian Saw Mill), has its facades decorated with beautiful typography. Our collective task will be to liberate it from its stone prison and release it to the world. During a three day workshop we'll work our way into rescuing this type from the walls into the screen.


**[Serriera Sobria](https://gitlab.com/manufacturaind/serreria-extravagante)**
 
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012, The participants of the "[Stone to Spaceship](http://manufacturaindependente.com/stonespaceship/)" workshop at Medialab Prado in July 2012, with Reserved Font Name 'Serreria Sobria'.*

A typeface designed collectively during the From Stone to Spaceship workshop, for Medialab Prado.



**[Station](https://fontlibrary.org/en/font/station)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Nick Matavka*

Based on the station markings on Toronto's tube network.


**[VTF Victorianna](http://www.velvetyne.fr/fonts/victorianna/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Jérémy Landes-Nones & Sébastien Hayez *

Thin roman & italicfont, inspired by english victorian types. 


**[Wire One](http://www.cyreal.org/2012/07/wire/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011, [Cyreal](www.cyreal.org), with Reserved Font Name "Wire One".*

Wire is a condensed monoline sans. Its modular-based characters are flavored with a sense of art nouveau. Nearly hairline thickness suggests usage for body text above 12px. While at display sizes it reveals its tiny dot terminals to create a sharp mood in headlines.


**[Znikomit](http://www.glukfonts.pl/font.php?font=Znikomit)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011, [gluk](gluksza@wp.pl),
with Reserved Font Znikomit.*



### Classic

Historic typefaces! Some font files were released as public domain by the original designers (Chicago), but most are Libre recreations of typefaces that are either officially, or *should be* in the public domain. eg. [Eric Gill](https://en.wikipedia.org/wiki/Eric_Gill) died in 1940, so his work should have become public domain in Europe in 2010. But a font is not just the drawings, it's also the software and automatic kerning etc, so that's what Monotype claims under exclusive © for Gill Sans. 'Sans Guilt' was recreated by tracing Eric Gill's early sketches. Futura Renner is a similar story.
Courier Prime is a modern update, to make scriptwriters' lives a litte prettier.


**[ChicagoFLF](http://christtrekker.users.sourceforge.net/fnt/chicago.shtml)**
*Public Domain*

Robin Casady created ChicagoFLF and placed it in the public domain. This version contains the following changes from the original: *Moved symbols to correct Unicode positions. *Incorporated additional symbols from CheckboxFLF, which used identical Basic Latin glyphs, and thus was essentially the "same" font with different symbols.


**[Courier Prime](http://quoteunquoteapps.com/courierprime/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), designed by Alan Dague-Greene for John August and Quote-Unquote Apps.*

Since the beginning, screenplays have been written in Courier. Its uniformity allows filmmakers to make handy comparisons and estimates, such as 1 page = 1 minute of screen time.
But there’s no reason Courier has to look terrible. We set out to make the best damn Courier ever.


**[DIN OSP](http://ospublish.constantvzw.org/foundry/osp-din/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011 [OSP](http://ospublish.constantvzw.org).*

The first cut of OSP-DIN was drawn for the festival Cinema du réel 2009, when we were invited to work on a cartographic version of the programme (see "OSP_cine-du-reel2009.jpg" in the documentation folder). We drew the first cut of the open source DIN from grid based drawings similar to the original 1932 drawings of the DIN we saw during our investigative trip to Berlin in february 2008.

**[Futura Renner](https://github.com/uplaod/FuturaRenner)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Bastien Sozoo*

Futura Renner family is a digital version of Futura lead character belonging to the typography department of Ecole Nationale Supérieure des Arts de la Cambre, and probably the first draft of Futura as we know it.

**[Goudy Bookletter 1911](https://www.theleagueofmoveabletype.com/goudy-bookletter-1911)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009, Barry Schwartz site-chieftain@crudfactory.com, with Reserved Font Name OFL "Goudy Bookletter 1911".*
Based on Frederic Goudy’s Kennerley Oldstyle.

A few words on why I think Kennerley Oldstyle is beautiful: In making this font, I discovered that Kennerley fits together tightly and evenly with almost no kerning. Thus the following words from Monotype specimen books are just: “[W]hen composed into words the characters appear to lock into one another with a closeness common in early types, but not so often seen in later-day creations.” These are letters that take command of the space around them; notice, for instance, the bowed shapes of the v and w.

**[Libre Baskerville](http://www.impallari.com/projects/overview/libre-baskerville)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Libre Baskerville is webfont optimized for web body text (typically 16px). It's based on 1941 ATF Specimens, but it has a taller x height, wider counters and minor contrast that allow it to work on small sizes in any screen.


**[Libre Bodoni](https://github.com/impallari/Libre-Bodoni)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

The Libre Bodoni fonts are based on the 19th century Morris Fuller Benton's ATF design, but specifically adapted for today's web requirements.

They are a perfect choice for everything related to elegance, style, luxury and fashion.


**[Libre Franklin](https://github.com/impallari/Libre-Franklin)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Libre Franklin is an interpretation and expancion based on the 1912 Morris Fuller Benton’s classic.


**[OFL Sorts Mill Goudy](https://www.theleagueofmoveabletype.com/sorts-mill-goudy)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009, [Barry Schwartz](site-chieftain@crudfactory.com), with Reserved Font Name OFL "Sorts Mill Goudy".*

A 'revival' of Goudy Oldstyle and Italic, with features including small capitals (in the roman only), oldstyle and lining figures, superscripts and subscripts, fractions, ligatures, class-based kerning, case-sensitive forms, and capital spacing. There is support for many languages using latin scripts.


**[Sans Guilt](http://ospublish.constantvzw.org/foundry/sans-guilt/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011 OSP* 

Versions of Gill Sans based on three different sources. 
Sans Guilt MB: Based on a rasterized pdf made with the Monotype Gill Sans delivered with Mac OSX. 
Sans Guilt DB: Based on early sketches by Eric Gill. 
Sans Guilt LB: Based on lead type from Royal College of Arts letterpress workshop.
