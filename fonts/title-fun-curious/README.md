### Title/Fun/Curious


Some are 'normal' fonts that only come in one weight, some have interesting stories behind them (eg Engineer Hand, Mill, Nanook, Serriera) and the 'fun' fonts are generally at the more useful end of the ¡FUN FONTS! spectrum.


**[Acidente](https://gitlab.com/manufacturaind/acidente)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2013, [Luís Camanho](luiscamanho@gmail.com), with Reserved Font Name 'Acidente'.*

Acidente was drafted in paper in 2009, to be used in a set of poster designs. The shapes are inspired by Akzidenz Grotesk.

In 2013 it was digitised, using Insckape and FontForge.


**[Antonio](https://www.fontsquirrel.com/fonts/antonio)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011-12, [vernon adams](vern@newtypography.co.uk), with Reserved Font Names ‘Antonio’*

Antonio is a reworking of a traditional advertising sans serif typeface. The letter forms have been digitised and then reshaped for use as a webfont, the counters have been opened up a little and the stems optimised for use as bold display font in modern web browsers.

**[Antwerp](http://www.citype.net/city/antwerp)**
*[Creative Commons - Attribution](https://creativecommons.org/licenses/by/3.0/) (CC BY 3.0) [Thijs Meulendijks](http://www.thijsmeulendijks.nl/)*

[Citype](http://www.citype.net/) is a project where designers create free typefaces inspired by a city. The typeface depicts the different cultures living together but apart in Antwerp. Its vertical appearance refers to the sometimes narrow-minded people and closed nature of people.
For some reason, the font file is called '2602.otf' and fails to install properly when renamed 'Antwerp.otf'. I don't know enough about fonts to fix this. A little help?


**[Arca Majora](https://hanken.co/product/arca-majora-2/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015, [Alfredo Marco Pradil](http://behance.net/pradil) (ammpradil@gmail.com), with Reserved Font Name ARCA MAJORA.*

Arca Majora is an all-uppercase geometric sans-serif that is great for headlines, logotypes and posters. Sharp tips and bold stems—Arca Majora is perfect for high-impact communication.


**[Beon](https://github.com/uplaod/Beon)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Bastien Sozoo*

A neon font


**[Engineer Hand](https://fontlibrary.org/en/font/engineer-hand)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Nick Matavka*

Architects and engineers use a particular, very regimented script for formal drawings. This is for legibility purposes and is known as Draftsman's Hand, Engineer's Hand, or Architect's Hand. The same script is often used in pre-1950's book illustrations, when they were all done by hand. This font attempts to replicate one such example, from an American War Department collection of ammunition drawings. The letter sizes are a bit irregular, but this font is still an excellent choice to label diagrams and technical blueprints for that hand-drawn, 1950's look.

**[Fachada](https://gitlab.com/manufacturaind/fachada)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2013, [Rui Silva](ruisilva@alfaiataria.org), with Reserved Font Name 'Fachada'.*

**[League Gothic](https://www.theleagueofmoveabletype.com/league-gothic)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, [Caroline Hadilaksono](caroline@hadilaksono.com) & [Micah Rich](micah@micahrich.com), with Reserved Font Name: "League Gothic".*

League Gothic is a revival of an old classic, and one of our favorite typefaces, Alternate Gothic #1. It was originally designed by Morris Fuller Benton for the American Type Founders Company in 1903. The company went bankrupt in 1993, and since the original typeface was created before 1923, the typeface is in the public domain.

We decided to make our own version, and contribute it to the Open Source Type Movement. Thanks to a commission from the fine & patient folks over at WND.com, it's been revised & updated with contributions from Micah Rich, Tyler Finck, and Dannci, who contributing extra glyphs.

**[League Spartan](https://www.theleagueofmoveabletype.com/league-spartan)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © September 22 2014, [Micah Rich](micah@micahrich.com), with Reserved Font Name: "League Spartan".*

A new classic, this is a bold, modern, geometric sans-serif that has no problem kicking its enemies in the chest.

Taking a strong influence from ATF's classic Spartan family, we're starting our own family out with a single strong weight. We've put a few unique touches into a beautiful, historical typeface, and made sure to include an extensive characterset – currently totaling over 300 glyphs.

**[LilGrotesk](https://github.com/uplaod/LilGrotesk)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Bastien Sozoo*

Lil Grotesk is a sans-serif typeface in two weights.

**[Matchbook](http://www.onebyfourstudio.com/projects/fonts/2009/matchbook-typefaces/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009, Brian Haines, [One by Four](info@onebyfourstudio.com)*

Matchbook is a simple and functional set of two typefaces we designed in a serif and sans-serif version. Each set includes all accented characters and works beautifully in larger scales.


**[Milano](http://www.citype.net/city/milano)**
*[Creative Commons - Attribution](https://creativecommons.org/licenses/by/3.0/) (CC BY 3.0) [Marco Oggian](http://www.citype.net/city/www.marcoggian.info)*

[Citype](http://www.citype.net/) is a project where designers create free typefaces inspired by a city. 
Milano is a noisy and chaotic city, with a little paradise area called Navigli. Navigli are little channels in the center of Milano, where you can chill yourself and drinking a beer seeing the beauty of the water. I love water.

**[Mill](http://osp.kitchen/foundry/mill/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012 OSP* 

Mill has been created for engraving building instructions into the wood of a bench.

**[Nanook](https://fontlibrary.org/en/font/nanook)**
*[SIL Open Font License](http://scripts.sil.org/OFL),  Lucas Le Bihan*

Based on the intertitle typeface in Robert Flaherty's early documentaries, like the groundbreaking [Nanook of the North](https://en.wikipedia.org/wiki/Nanook_of_the_North)
[PDF](https://github.com/LucasLeBihan/Nanook/blob/master/documentation/PRESENTATION.pdf)

**[Ostrich Sans](https://www.theleagueofmoveabletype.com/ostrich-sans)**
*[SIL Open Font License](http://scripts.sil.org/OFL),  © 2011, (Tyler Finck](hi@tylerfinck.com), with Reserved Font Name: "Ostrich Sans".*

A gorgeous modern sans-serif with a very long neck. With a whole slew of styles & weights:


**[Oswald](https://www.fontsquirrel.com/fonts/oswald)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012, [Vernon Adams](vern@newtypography.co.uk), with Reserved Font Names ‘Oswald’*

**[Pecita](http://pecita.eu/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © © 2009, [Philippe Cochy](http://pecita.com) with Reserved Font Name Pecita.*

Pecita is a typeface that mimics handwriting. The use of complex features makes this possible. The font is coded with full Unicode glyphs customary in Latin and pan European languages, the phonetic alphabet, katakana, mathematical operators and many symbols. 

**[Playfair Display](http://www.forthehearts.net/typeface-design/playfair-display/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Claus Eggers Sørensen*

Play­fair is a trans­itional design. From the time of enlight­en­ment in the late 18th cen­tury, the broad nib quills were replaced by poin­ted steel pens. This influ­enced typo­graph­ical let­ter­forms to become increas­ingly detached from the writ­ten ones. Devel­op­ments in print­ing tech­no­logy, ink and paper mak­ing, made it pos­sible to print let­ter­forms of high con­trast and del­ic­ate hairlines.

This design lends itself to this period, and while it is not a revival of any particular design, it takes influence from the designs of printer and typeface designer John Baskerville, the punchcutter William Martin’s typeface for the ‘Boydell Shakspeare’ (sic) edition, and from the ‘Scotch Roman’ designs that followed thereafter.


**[Pompiere](http://karolinalach.com/194342/2077414/work/pompiere-typeface)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Karolina Lach*

Pompiere is a narrow sans serif typeface that takes its inspiration from a handlettered sign found outside of a firehouse in New York City. It combines the qualities of delicate, handwritten craftsmanship with subtle Art Nouveau-inspired details and proportions. These characteristics combine to lend the typeface a friendly, unassuming atmosphere.


**[Serriera Sobria](https://gitlab.com/manufacturaind/serreria-sobria)**
 
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012, The participants of the "[Stone to Spaceship](http://manufacturaindependente.com/stonespaceship/)" workshop at Medialab Prado in July 2012, with Reserved Font Name 'Serreria Sobria'.*

A typeface designed collectively during the From Stone to Spaceship workshop, for Medialab Prado.
The soon to be home of Medialab-Prado, Serrería Belga (Old Belgian Saw Mill), has its facades decorated with beautiful typography. Our collective task will be to liberate it from its stone prison and release it to the world. During a three day workshop we'll work our way into rescuing this type from the walls into the screen.


**[Serriera Sobria](https://gitlab.com/manufacturaind/serreria-extravagante)**
 
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012, The participants of the "[Stone to Spaceship](http://manufacturaindependente.com/stonespaceship/)" workshop at Medialab Prado in July 2012, with Reserved Font Name 'Serreria Sobria'.*

A typeface designed collectively during the From Stone to Spaceship workshop, for Medialab Prado.



**[Station](https://fontlibrary.org/en/font/station)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Nick Matavka*

Based on the station markings on Toronto's tube network.


**[VTF Victorianna](http://www.velvetyne.fr/fonts/victorianna/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Jérémy Landes-Nones & Sébastien Hayez *

Thin roman & italicfont, inspired by english victorian types. 


**[Wire One](http://www.cyreal.org/2012/07/wire/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011, [Cyreal](www.cyreal.org), with Reserved Font Name "Wire One".*

Wire is a condensed monoline sans. Its modular-based characters are flavored with a sense of art nouveau. Nearly hairline thickness suggests usage for body text above 12px. While at display sizes it reveals its tiny dot terminals to create a sharp mood in headlines.


**[Znikomit](http://www.glukfonts.pl/font.php?font=Znikomit)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011, [gluk](gluksza@wp.pl),
with Reserved Font Znikomit.*

