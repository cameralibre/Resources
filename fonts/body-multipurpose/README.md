### Body / Multipurpose


Useful Sans or Serif styles, in multiple weights, including italics wherever possible:


**[Aileron](http://dotcolon.net/font/aileron/)**
*[CC0 (No Rights Reserved)](https://creativecommons.org/choose/zero/)m By Sora Sagano*

Aileron is a typeface that is classified as neo-grotesque, including Helvetica as a reference, is a sans-serif font that summarizes add the interpretation of my own.

The spread between the slightly shaped as an adjustment to the body for, in order to facilitate the identification of the case-eye "I", we have to design that curve the distal portion of the lower case El "l".

In addition, the dot part of, such as "i" and "j" or period in a circle, by performing a process with an awareness of clothoid curve to curve part, was to get an overall soft impression. The design basis is close to Helvetica, but conceptually it has become rather close to Univers.

In addition, we are for the first time provide a italic as the font of this dot colon. Basic is what tilted mechanically, but has undergone a visual correction so that there is no sense of incongruity.

Taking advantage of the multiple master, weights are available 8 stage from UltraLight to Black. I think that you can use in a wide scene.

Please read about the dot colon fonts when you use the font.

**[Aleo](https://www.behance.net/gallery/ALEO-Free-Font-Family/8018673)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Alessio Laiso*

Aleo is a contemporary typeface designed by Alessio Laiso as the slab serif companion to the Lato font by Łukasz Dziedzic. Aleo has semi-rounded details and a sleek structure, giving it a strong personality while still keeping readability high. The family comprises six styles: three weights (light, regular and bold) with corresponding true italics.

**[Arvo](https://www.google.com/fonts/specimen/Arvo)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Anton Koovit*

 Arvo is a geometric slab-serif typeface family suited for screen and print. The flavour of the font is rather mixed. Its monolinear-ish, but has a tiny bit of contrast (which increases the legibility a little in Mac OS X.)

The name Arvo is a typical Estonian man's name, but is not widely used today. In the Finnish language, Arvo means "number, value, worth." Considering how much programming is involved in hinting, all these meanings are true. 


**[Belgika](http://osp.kitchen/foundry/belgica-belgika/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), OSP / Pierre Huyghebaert](http://www.speculoos.com), pierre@speculoos.com*

A majuscule sans-serif font from OSP with uniform stroke widths, based on sundry generic sources.

**[Bellota](http://www.pixilate.com/font/bellota/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Kemie Guaida*

Based on the font “Snippet” by Gesine Todt, Bellota is an ornamented, low contrast sans-serif. It’s just cute enough!

**[Cantarell](https://git.gnome.org/browse/cantarell-fonts/tree/README)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2009--2016, [The Cantarell Authors](https://git.gnome.org/browse/cantarell-fonts/tree/CONTRIBUTORS)*

The Cantarell typeface family is a contemporary Humanist 
sans serif, and is used by the GNOME project for its user
interface and the Fedora project. 

**[Cormorant](https://www.behance.net/gallery/28579883/Cormorant-an-open-source-display-font-family)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Christian Thalmann, [Catharsis Fonts](https://www.myfonts.com/foundry/Catharsis_Fonts)*

Cormorant is an original design for an extravagant display serif typeface inspired by the Garamond heritage, hand-drawn and produced by Catharsis Fonts. While traditional Garamond cuts make for exquisite reading at book sizes, they appear clumpy and inelegant at larger sizes. The design goal of Cormorant was to distill the æsthetic essence of Garamond, unfetter it from the limitations of metal printing, and allow it to bloom into its natural refined form at high definition.

Cormorant is characterized by scandalously small counters, razor-sharp serifs, dangerously smooth curves, and flamboyantly tall accents. While many implementations of Garamond at small optical sizes already exist (including the open-sourced EB Garamond by Georg Duffner), Cormorant aims for the sparsely populated niche of display-size counterparts that exploit the high resolution of contemporary screens and print media to the fullest.


**[Dosis](http://www.impallari.com/dosis)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Dosis is a rounded sans-serif type family. It started with the Extra Light style, useful only at size 36pt or upm and the Extended Latin character set included many alternative characters, all designed by Edgar Tolentino and Pablo Impallari.


**[Encode](http://www.impallari.com/projects/overview/encode)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Pablo Impallari*

Some letters have abrupt asymmetrical curves, while other are soft and fluid. This contrasting combination allows for a pleasant modern feeling, without being squarish, boring or repetitive.
What started as a single style font, is now becoming a 45 styles super-family.


**[Fira Sans](http://mozilla.github.io/Fira/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), Digitized data © 2012-2015, The Mozilla Foundation and Telefonica S.A. with Reserved Font Name "Fira" * 

Mozilla's font, originally designed for the Firefox OS project

**[Gentium](http://software.sil.org/gentium/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2003-2014 SIL International (http://www.sil.org/), with Reserved Font Names "Gentium" and "SIL".*

Gentium is a typeface family designed to enable the diverse ethnic groups around the world who use the Latin, Cyrillic and Greek scripts to produce readable, high-quality publications. It supports a wide range of Latin- and Cyrillic-based alphabets. [Gentium on Wikipedia](https://en.wikipedia.org/wiki/Gentium).

**[Hanken](https://fontlibrary.org/en/font/hanken)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015, [Alfredo Marco Pradil](http://behance.net/pradil), ammpradil@gmail.com, with Reserved Font Name Hanken.*

Geometric and rounded font.

**[HK Grotesk](https://hanken.co/product/hk-grotesk/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015, [Alfredo Marco Pradil](http://behance.net/pradil), ammpradil@gmail.com, with Reserved Font Name HK Grotesk.*

HK Grotesk is a sans serif typeface inspired from the classical grotesques. The goal in designing HK Grotesk is to create a more friendly and distinguishable typeface that is suitable for small text.

**[Josefin Sans](https://www.google.com/fonts/specimen/Josefin+Sans)**
*[SIL Open Font License](http://scripts.sil.org/OFL), [Santiago Orozco](http://typemade.mx/)*

The idea for creating this typeface was to make it geometric, elegant and kind of vintage, especially for titling. It is inspired by Rudolf Koch's Kabel (1927), Rudolf Wolf's Memphis (1930), Paul Renner's Futura (1927).
My idea was to draw something with good style, that reflects Swedish design and their passion for a good lifestyle, and by default all other Scandinavian styles.
Something weird happened when I drew the letter "z": when I took my typography courses in college, I saw a very interesting typeface in a book, the New Universal Typeface "Newut" from André Baldinger (which I have loved since then), and after I finished the "z" I ran into Newut' site again and noticed that I had unconsciously drawn it with the same haircut.
The x-height is half way from baseline to caps height, unlike any other modern typeface.
I wanted to do something different with the ampersand, so I made three and will include them as alternates in future versions. This version also includes Old Style Numerals.

**[Josefin Slab](https://www.google.com/fonts/specimen/Josefin+Slab)**
*[SIL Open Font License](http://scripts.sil.org/OFL), [Santiago Orozco](http://typemade.mx/)*

Josefin Slab was the first typeface–at least in my mind–I designed! But I decided to start simple with Josefin Sans. Following the 1930s trend for geometric typefaces, it just came to me that something between Kabel and Memphis with modern details will look great.

I wanted to stick to the idea of Scandinavian style, so I put a lot of attention to the diacritics, especially to "æ" which has loops connecting in a continuous way, so the "e" slope was determined by this character.

It also has some typewriter style attributes, because I've liked the Letter Gothic typeface since I was in high school, and that's why I decided to make a Slab version of Josefin Sans.

**[Lato](http://www.latofonts.com/lato-free-fonts)**
*[SIL Open Font License](http://scripts.sil.org/OFL), [Łukasz Dziedzic](http://www.latofonts.com/team/)*

Lato is a sanserif type­face fam­ily designed in the Sum­mer 2010 by Warsaw-​​based designer Łukasz Dziedzic (“Lato” means “Sum­mer” in Pol­ish). In Decem­ber 2010 the Lato fam­ily was pub­lished under the open-​​source Open Font License by his foundry tyPoland, with sup­port from Google.

In the last ten or so years, during which Łukasz has been designing type, most of his projects were rooted in a particular design task that he needed to solve. With Lato, it was no different. Originally, the family was conceived as a set of corporate fonts for a large client — who in the end decided to go in different stylistic direction, so the family became available for a public release.

When working on Lato, Łukasz tried to carefully balance some potentially conflicting priorities. He wanted to create a typeface that would seem quite “transparent” when used in body text but would display some original traits when used in larger sizes. He used classical proportions (particularly visible in the uppercase) to give the letterforms familiar harmony and elegance. At the same time, he created a sleek sans serif look, which makes evident the fact that Lato was designed in 2010 — even though it does not follow any current trend.

The semi-rounded details of the letters give Lato a feeling of warmth, while the strong structure provides stability and seriousness. “Male and female, serious but friendly. With the feeling of the Summer,” says Łukasz.

**[Linux Libertine](http://www.linuxlibertine.org)**
*[GNU GPL General Public License version 2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) (GPLv2), [SIL Open Font License](http://scripts.sil.org/OFL)*

We work on a versatile font family. It is designed to give you an alternative for fonts like T*mes New Roman. 


**[Lora](http://www.cyreal.org/2012/07/lora/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2011 Olga Karpushina*

Lora is a well-balanced contemporary serif with roots in calligraphy. It is a text typeface with moderate contrast well suited for body text.

A paragraph set in Lora will make a memorable appearance because of its brushed curves in contrast with driving serifs. The overall typographic voice of Lora perfectly conveys the mood of a modern-day story, or an art essay.


**[Montserrat](https://www.google.com/fonts/specimen/Montserrat)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2012 Julieta Ulanovsky*

The old posters and signs in the traditional neighborhood of Buenos Aires called Montserrat inspired me to design a typeface that rescues the beauty of urban typography from the first half of the twentieth century. The goal is to rescue what is in Montserrat and set it free, under a free, libre and open source license, the [SIL Open Font License](http://scripts.sil.org/OFL).

As urban development changes this place, it will never return to its original form and loses forever the designs that are so special and unique. To draw the letters, I rely on examples of lettering in the urban space. Each selected example produces its own variants in length, width and height proportions, each adding to the Montserrat family. The old typographies and canopies are irretrievable when they are replaced.

There are other revivals, but those do not stay close to the originals. The letters that inspired this project have work, dedication, care, color, contrast, light and life, day and night! These are the types that make the city look so beautiful.

**[PropCourierSans](http://manufacturaindependente.org/propcourier-sans/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2013 Manufactura Independente*

PropCourier is the canonical typeface for Libre Graphics magazine. It was created through a conversion of OSP's NotCourier Sans from a monospaced font into a proportional one.

From there, we've been refining the design and releasing new versions with each issue of Libre Graphics magazine, until we settled on a final design when we released issue 2.2 of the magazine.

PropCourier-Sans is a fork of Open Source Publishing’s [Not-Courier Sans](http://osp.kitchen/foundry/notcouriersans/), using proportional spacing and some finer tweaks. 12 Official typeface for the Libre Graphics Magazine. PropCourier-Sans is available in three weights: Regular, Medium and Bold.
NotCourier was designed by OSP (Ludivine Loiseau), in July 2008. It is based on Nimbus Mono, © 1999 by (URW)++ Design & Development; Cyrillic glyphs added by Valek Filippov (C) 2001-2005.

**[Open Sans](https://www.google.com/fonts/specimen/Open+Sans)**
*[Apache License](http://www.apache.org/licenses/LICENSE-2.0.html), Steve Matheson*

Open Sans is a humanist sans serif typeface designed by Steve Matteson, Type Director of Ascender Corp. This version contains the complete 897 character set, which includes the standard ISO Latin 1, Latin CE, Greek and Cyrillic character sets. Open Sans was designed with an upright stress, open forms and a neutral, yet friendly appearance. It was optimized for print, web, and mobile interfaces, and has excellent legibility characteristics in its letterforms.

**[Overpass](http://overpassfont.org/)**
*Dual Licensed under the [SIL Open Font License](http://scripts.sil.org/OFL) and also the [LGPL 2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html), © 2015 Red Hat, Inc.*

Overpass is the typeface used by Red Hat. The origin-artwork that inspired Overpass (Highway Gothic) is public domain.
Used globally in road signage, Highway Gothic embodies design for the public good.
Traffic control systems demonstrate that open standards and open communication help complex communities work better together.
This digital interpretation of Highway Gothic includes a wide array of variations so that web designers have the flexibility they expect for modern web typography.
Today’s enterprise brands all have distinct typographic expressions. In the software arena, Overpass is strongly aligned to Red Hat brand.

**[PT Sans](https://www.google.com/fonts/specimen/PT+Sans)**
*[SIL Open Font License](http://scripts.sil.org/OFL) © 2010, [ParaType Ltd.](http://www.paratype.com/public), with Reserved Font Names "PT Sans" and "ParaType".*

PT Sans was developed for the project "Public Types of Russian Federation." The main aim of the project is to give possibility to the people of Russia to read and write in their native languages.

The project is dedicated to the 300 year anniversary of the civil type invented by Peter the Great in 1708–1710. It was given financial support from the Russian Federal Agency for Press and Mass Communications.

The fonts include standard Western, Central European and Cyrillic code pages, plus the characters of every title language in the Russian Federation. This makes them a unique and very important tool for modern digital communications.

PT Sans is based on Russian sans serif types of the second part of the 20th century, but at the same time has distinctive features of contemporary humanistic designs.


**[PT Serif](https://www.google.com/fonts/specimen/PT+Serif)**
*[SIL Open Font License](http://scripts.sil.org/OFL) © 2010, [ParaType Ltd.](http://www.paratype.com/public), with Reserved Font Names "PT Sans" and "ParaType".*

PT Serif™ is the second pan-Cyrillic font family developed for the project “Public Types of the Russian Federation.”


**[Raleway](https://www.theleagueofmoveabletype.com/raleway)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, Matt McInerney matt@pixelspread.com, with Reserved Font Name: "Raleway".*

Raleway is an elegant sans-serif typeface, designed in a single thin weight. It is a display face that features both old style and lining numerals, standard and discretionary ligatures, a pretty complete set of diacritics, as well as a stylistic alternate inspired by more geometric sans-serif typefaces than it's neo-grotesque inspired default character set.


**[Rubik](http://hubertfischer.com/work/type-rubik)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2015 by Hubert & Fischer. 

Google Creative Lab approached us to design a typeface for the branding of the Rubik's Cube Exhibition ”Beyond Rubik's Cube“ at the Liberty Science Center, Jersey City. We designed a slightly rounded heavyweight font in which the letters fit perfectly in a single cubelet of the Rubik's Cube. The font was expanded to include Cyrillic and Hebrew characters for the exhibition, which will travel internationally.


**[Source Sans Pro](http://adobe-fonts.github.io/source-sans-pro/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, 2012 [Adobe Systems Incorporated](http://www.adobe.com/), with Reserved Font Name 'Source'. All Rights Reserved. Source is a trademark of Adobe Systems Incorporated in the United States and/or other countries.*

Sans serif font family for user interface environments. 


**[Source Serif Pro](http://adobe-fonts.github.io/source-serif-pro/)**
*[SIL Open Font License](http://scripts.sil.org/OFL), © 2010, 2012 [Adobe Systems Incorporated](http://www.adobe.com/), with Reserved Font Name 'Source'. All Rights Reserved. Source is a trademark of Adobe Systems Incorporated in the United States and/or other countries.*

Serif typeface designed to complement Source Sans Pro. 


**[Yanone Kaffesatz](http://www.yanone.de/fonts/kaffeesatz/)**
*[CC BY 2.0 DE](https://creativecommons.org/licenses/by/2.0/de/) Jan Gerner*

Yanone Kaffeesatz was first published in 2004 and is Yanone’s first ever finished typeface. Its Bold is reminiscent of 1920s coffee house typography, while the rather thin fonts bridge the gap to present times. Lacking self confidence and knowledge about the type scene, Yanone decided to publish the family for free under a Creative Commons License. A decision that should turn out one of the best he ever made. It has been downloaded over 100,000 times to date, and you can witness Kaffeesatz use on German fresh-water gyms, Dubai mall promos and New Zealand McDonalds ads. And of course on coffee and foodstuff packaging and café design around the globe

