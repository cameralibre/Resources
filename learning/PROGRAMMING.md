# PROGRAMMING LEARNING RESOURCES

**Book / Online learning**

[Learn Python the Hard Way](http://learnpythonthehardway.org/book/), by Zed Shaw

[Introduction to BASH Programming](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html), by Mike G

**Offline learning**

look out for [OpenTechSchool](http://www.opentechschool.org/) workshops in [a town near you](http://www.opentechschool.org/locator/)!

**Physical products**

The [Arduino/Genuino Starter Kit](http://www.arduino.cc/en/Main/ArduinoStarterKit)
