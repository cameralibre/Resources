# Learning to use the vim text editor


As I spend a lot of my time creating documentation and writing in Markdown, I'm currently learning [vim](http://en.wikipedia.org/wiki/Vim_(text_editor)), to see if I like it and if it speeds up my workflow.

Right now I'm still learning, so it's horribly, painfully slow and awkward. But I can see a lot of potential! Let's see how it goes.

**[Vim Adventures](http://vim-adventures.com)**

This is the very best first step possible - get familiar with the navigation and commands of vim by playing a simple game. After finishing level 3 you have to pay $25 to unlock the rest of the game, but even those first 3 levels are really useful. 

**http://bilalquadri.com/blog/2012/12/17/getting-started-with-vim/**
