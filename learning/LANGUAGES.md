# Useful resources for language learning

**[Anki](http://ankisrs.net/)** 
A [spaced repetition](https://en.wikipedia.org/wiki/Spaced_repetition) flashcard system.
 Anki is a program which makes remembering things easy. Because it's a lot more efficient than traditional study methods, you can either greatly decrease your time spent studying, or greatly increase the amount you learn.

Anyone who needs to remember things in their daily life can benefit from Anki. Since it is content-agnostic and supports images, audio, videos and scientific markup (via LaTeX), the possibilities are endless.


**[Learning with Texts](https://sourceforge.net/projects/lwt/)**

Learning with Texts (LWT) is a tool for Language Learning, inspired by Stephen Krashen's principles in Second Language Acquisition, Steve Kaufmann's LingQ System and ideas from Khatzumoto (AJATT). Read & listen, save & test words in context!


**[Tatoeba](http://tatoeba.org)**

Tatoeba is a collection of sentences and translations, developed by an active community. It's collaborative, open, free and even addictive.


