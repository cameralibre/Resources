# Getting used to GNU/Linux

You've installed your new operating system! great! 

When you're just starting out, it seems like there's a lot to learn at first. Well, there is, but it's useful stuff to learn, and it's not that hard! You don't need to be able to program or anything, but it's good to have a general understanding of what is going on in your computer.

I will periodically add more to this!

**Almost everythng you need: [linuxjourney.com](https://linuxjourney.com)**

**[What are all those folders in / actually there for?](http://www.thegeekstuff.com/2010/09/linux-file-system-structure/)**

(and [related diagram](https://i.reddituploads.com/87ad3b41868c4073925ab91aac72fb37?fit=max&h=1536&w=1536&s=8ff8f3e3026db07035ab6040105cb19b))

**[The Art of the Command Line](https://github.com/jlevy/the-art-of-command-line)**


# How to solve problems or ask for help

If you have any problems while starting out, there's are a few different ways of solving the problem.

1. [Internet search](https://duckduckgo.com/) - copy and paste the error message, or describe the problem you're having in specific terms - i.e. don't search for *"computer broken, Linux sucks."* Instead, something like *"Ubuntu 16.04 screen won't suspend"* should narrow down the search results to something relevant.

2. Ask a friend who knows their way around this stuff for help. 

3. If you think you can describe the problem pretty specifically, ask in a community forum, eg the forum of the application that you are having trouble with, or your distribution's forum (such as [ubuntuforums.org](http://ubuntuforums.org/))

4. If you're not even sure what the problem is and how to describe it to other people, then you can ask the friendly, patient folk at the [Linux4Noobs subreddit](https://www.reddit.com/r/linux4noobs) for help. They'll be nice! (Don't ask in [r/linux](https://www.reddit.com/r/linux), I can't guarantee that they'll be nice...)

The general rules when posting a question on a community forum are:

- **Provide an informative, specific post title:** "Please help!" isn't helpful. "Shortcut for opening Terminal doesn't work" is more helpful, and it will encourage those with the right expertise to click on the link, read your question and help you to solve it.

- **be polite and concise, and try to explain your situation clearly:**
what's the problem? is there a particular action that triggers it? has this always happened, or is this behaviour new?

- **provide system information:**
Linux can run on anything from a supercomputer, to a Raspberry Pi, to a refrigerator. Some software issues will be dependent on your hardware, and your distribution. Providing information helps get better hekp, faster. For me, this would be:

**Computer Make & Model:** Lenovo s440

**Distribution:** Kubuntu 16.04

**Application version (if problem is with a specific application):** GIMP 2.8.16

you can find out the version for most programs from the command line, just type in the name of the program followed by --version. in some cases this is -version, not --version, just to be confusing. eg:

    sam@sam-S440:~$ gimp --version

    GNU Image Manipulation Program version 2.8.16

**Relevant hardware information:**

For example, if my problem is with the display, if images are tearing, things are flickering etc, the issue may be my graphics driver.
To find out my graphics driver, I can probably find it via the GUI system settings somewhere, but you can also use the terminal: 

    sudo lshw -class display

this command says "**do** as **s**uper-**u**ser: **l**i**s**t **h**ard**w**are devices, but only one **class** of hardware: **display** class (i.e. graphics controllers)

In my case, I get:  

       description: VGA compatible controller
       product: Haswell-ULT Integrated Graphics Controller
       vendor: Intel Corporation
       physical id: 2
       bus info: pci@0000:00:02.0
       version: 09
       width: 64 bits
       clock: 33MHz
       capabilities: msi pm vga_controller bus_master cap_list rom
       configuration: driver=i915 latency=0
       resources: irq:44 memory:f0000000-f03fffff memory:e0000000-efffffff ioport:5000(size=64)



