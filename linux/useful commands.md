## PDF Toolkit ([pdftk](http://linux.die.net/man/1/pdftk))

**concatenate multiple PDFs into a single PDF**

    pdftk file1.pdf file2.pdf cat output output_file.pdf

**concatenate certain pages from multiple PDFs into a single PDF**

    pdftk A=one.pdf B=two.pdf cat A1-7 B1-5 A8 output combined.pdf

*'A' and 'B' are here used as 'handles' - like variables in other forms of programming.*


## [Find](http://linux.die.net/man/1/find) 

**Find and move**

    find -name '*.webm' -exec mv '{}' ~/Videos/webm ';'

in this folder and its child folders, find all files with **.webm** in their name and move them to the folder '~/Videos/webm'

You can replace .webm with another filetype or a different part of a name that you're looking for, and if you want to copy the files rather than move them, just replace **mv** with **cp**.


## [Convert](http://linux.die.net/man/1/convert) (imagemagick)

**convert a lossless PNG to a compressed JPEG**

    convert input.png output.jpg

easy! you can tweak the settings if you really like, but the defaults work pretty well.

## [AWK]

**delete every nth line of a file (and write remainder to a new file)**

    awk 'NR%4' input.txt > output.txt

This will omit every **4th** line (line numbers that are a multiple of 4)and print the remaining lines to 'output.txt' - this kind of operation is useful for translations, for example, where a pattern is repeated throughout a document.
