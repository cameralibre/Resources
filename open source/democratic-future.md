This is a subjective, imperfect and partial list of some of the organisations, ideas and projects which are interlocking pieces of a more empowering, democratic society. There is no single idea which should be worked on first, we need transformations in each field and we need people putting their time in wherever they have the need or desire to help out. Some are working on 'big' problems, others smaller. 

My loose criteria for inclusion on this list are: 

- Does this idea/project empower and educate those who engage with it to have more knowledge and agency about the topic?
- Does it provide easy access to tools, knowledge and support to a broad base of citizens, not just those with money, connections or institutional backing?
- Does it use a more democratic form of governance or ownership that traditional models which tend toward inequality?
- is it transparent, evidence-based and accountable?

Not all of these projects fulfill all criteria, and the list is at a very, very early stage. Most projects make their tools and knowledge available [open source](https://en.wikipedia.org/wiki/Open_source) for anyone to use. Suggestions welcome via pull request or [twitter](https://twitter.com/cameralibre).

### Architecture & Housing
[WikiHouse](http://www.wikihouse.cc)  
['Scaling the Citizen Sector' whitepaper](https://medium.com/@AlastairParvin/scaling-the-citizen-sector-20a20dbb7a4c)  
[Open Building Institute](http://openbuildinginstitute.org)  
[BitHouse](https://www.bithouse.io/)  
[Embassy Network](https://embassynetwork.com/)  
[Open Architecture Collaborative](http://www.openarchcollab.org/about-us)  

### Art
[Ampliative Art](http://www.ampliativeart.org/en/welcome)  

### Activism
[Tactical Technology Collective](https://tacticaltech.org)  
[Rise Up](https://riseup.net)  
[CrowdVoice](http://crowdvoice.org/)  
 
### Agriculture & Food
[Community-Supported Agriculture](http://urgenci.net/)  
[New Harvest](http://www.new-harvest.org/)  
[Permaculture](https://en.wikipedia.org/wiki/Permaculture)  
[GreenWave](greenwave.org/about-us)  
[The Food Assembly](https://thefoodassembly.com/en)  

### Basic Income
[Mein Grundeinkommen](https://www.mein-grundeinkommen.de/blog/1282)

### Climate change 
[Project Drawdown](http://www.drawdown.org)  
[Resilience.io](http://resilience.io)  

### Cinema 
[Blender Open Movies](https://www.blender.org/institute)  
[Apertus Cinema Camera Ecosystem](https://www.apertus.org)  
[Female Filmmakers Initiative](https://womeninfilm.org/ffi)  
[Members' Media](http://www.membersmedia.net/about)

### Circular economy 
[Open Source Circular Economy days](https://oscedays.org)  _*full disclosure, I'm part of the team behind this one :)_  

### Civic Tech
[g0v](http://g0v.asia)

### Consumer electronics
[Fairphone](https://www.fairphone.com)  

### Corporations
[B Corporations](http://www.bcorporation.net/)  

### Data 
[Open Knowledge](https://okfn.org)  
[Open Data Institute](http://theodi.org)  

### Debt
[Debt Collective](https://debtcollective.org/)  

### Decision-making 
[Loomio](https://www.loomio.org)  

### Design
[Open Structures](http://beta.openstructures.net/pages/2#vraag-1a)

### E-commerce 
[FairMondo](https://www.fairmondo.de/global)  

### Energy
[Energy Co-ops](https://en.wikipedia.org/wiki/Utility_cooperative)  
[European Federation for Renewable Energy Cooperatives](https://rescoop.eu/)  

### Extractive industries
[Open Oil](http://openoil.net)  

### Fashion/Consumer Products
[Impossible](http://buyimpossible.com/pages/about-us)

### Journalism 
[Knight Foundation](http://www.knightfoundation.org)  
[Citizen Desk](https://www.sourcefabric.org/en/citizendesk)  
[Banyan Project](http://banyanproject.coop)  

### Health & Social Care
[OpenCare](http://opencare.cc/)  
[Buurtzorg](https://translate.google.com/translate?sl=auto&tl=en&js=y&prev=_t&hl=en&ie=UTF-8&u=http%3A%2F%2Fwww.buurtzorgnederland.com%2F&edit-text=&act=url)  

### Internet access & infrastructure 
[Freifunk](https://freifunk.net/en)  
[Guifi Net](https://guifi.net/en)  

### Knowledge
[Wikimedia](https://www.wikimedia.org)  
[Just for the Record](http://justfortherecord.space)  
[Archive.org](https://archive.org/)  

### Legal
[Creative Commons](https://creativecommons.org/)  
[Legal Hackers](http://legalhackers.org/)  

### Manufacturing 
[Fab Labs](https://www.fablabs.io)  

### Music 
[Resonate](https://resonate.is)  
[CASH Music](https://cashmusic.org)  
[Free Music Archive](http://freemusicarchive.org/about)  

### Money in commons (good!)
[Cobudget](http://cobudget.co)  
[Balance](http://balance.sharett.org)  
[Robin Hood Coop](http://robinhoodcoop.org/)  
[Liberapay](https://liberapay.com/)  
[FairCoin](https://fair-coin.org/)  
[Gratipay](https://gratipay.com/)  

### Money in politics (bad!)
[Rootstrikers](http://www.rootstrikers.org)  
[Represent.us](https://represent.us)  
[Equal Citizens](http://www.equalcitizens.us/)  

### [Platform] Cooperatives 
[Internet of Ownership](http://internetofownership.net)  
[#WeAreTwitter](https://www.change.org/p/twitter-inc-free-twitter-from-wall-street)  
[FairShares](http://www.fairshares.coop)  
[Common Futures](http://commonfutures.eu/)  

### Political engagement
[d-cent](http://dcentproject.eu)  
[DiEm25](https://diem25.org/)  
[Pirate Parties](https://en.wikipedia.org/wiki/Pirate_Party)  

### Radio
[WFMU](https://wfmu.org/about/)

### Science 
[Public Laboratory for Open Technology and Science](https://publiclab.org)  
[La Paillasse](http://lapaillasse.org)  
[Center for Open Science](https://centerforopenscience.org)  
[Open Science Framework](https://osf.io)  
[Open Access movement](https://en.wikipedia.org/wiki/Open_access)  
[Public Library of Science](https://www.plos.org/)  

### Software/digital rights
[Free Software Foundation](http://www.fsf.org)  
[Electronic Frontier Foundation](https://www.eff.org)  
[CryptoParty](https://en.wikipedia.org/wiki/CryptoParty)  
[International Modern Media Institute](https://en.immi.is/)

### Sustainable hardware 
[Good Tech](http://goodtech.cc)  
[Open Source Ecology](http://opensourceecology.org)  
[Appropedia](http://www.appropedia.org)  
[POC21](http://poc21.cc)

### Repair 
[The Restart Project](https://therestartproject.org)  

### Transport/Rideshare
[Arcade City](https://arcade.city)  

### Travel 
[BeWelcome](https://www.bewelcome.org)  

### Uncategorized organisations
[Enspiral](http://enspiral.com/)  
[OuiShare](http://ouishare.net/en)  
[P2P Foundation](http://p2pfoundation.net)  
[Waag Society](http://waag.org/en)  



