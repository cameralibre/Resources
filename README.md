# [/fonts](https://gitlab.com/cameralibre/Resources/tree/master/fonts)

A collection of my favourite libre fonts, from the day-to-day use of **[Overpass](http://overpassfont.org/)** or **[HK Grotesk](https://hanken.co/product/hk-grotesk/)**, old-style blackletter like **[JustLetters](https://fontlibrary.org/en/font/just-letters)**, rejigged classics like **[Futura Renner](https://github.com/uplaod/FuturaRenner)**, modular fonts like **[Amalgame](https://fontlibrary.org/en/font/amalgame)**, monospaced fonts like **[Source Code Pro](http://adobe-fonts.github.io/source-code-pro/)** for code, and then all sorts of interesting stuff like **[Antwerp](http://www.citype.net/city/antwerp)** and **[Nanook](https://fontlibrary.org/en/font/nanook)** too.

# [/learning](https://gitlab.com/cameralibre/Resources/tree/master/learning)

(Mostly) libre learning resources and links for programming, learning the command line, or learning languages.

# [/linux](https://gitlab.com/cameralibre/Resources/tree/master/linux)

Collection of useful terminal commands, links to informative posts, and information to both to help people who are just starting out with using GNU/Linux, and to serve as a reference whenever I need to relearn or remember something.

# [/open source](https://gitlab.com/cameralibre/Resources/blob/master/open source)

resources for learning about existing initiatives or building communities around open source projects.

# [/video](https://gitlab.com/cameralibre/Resources/tree/master/video)

Information to help improve planning, shooting and editing of video, with links to useful resources, and commands for the all-powerful FFmpeg program.
